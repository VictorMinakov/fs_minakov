package ru.cft.focusstart.task2.shape;

public class Rectangle extends Shape {

    private double sideA;
    private double sideB;

    public Rectangle(double sideA, double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    @Override
    protected String getName() {
        return "прямоугольник";
    }

    @Override
    public double getArea() {
        return sideA * sideB;
    }

    @Override
    public double getPerimeter() {
        return 2.0 * (sideA + sideB);
    }

    @Override
    public String getStringInformation() {
        return super.getStringInformation() +
                "Диагональ: " + getDiagonal() + "\n" +
                "Длина: " + getLength() + "\n" +
                "Ширина: " + getWidth();
    }

    double getDiagonal() {
        return Math.sqrt(sideA * sideA + sideB * sideB);
    }

    double getLength() {
        return Math.max(sideA, sideB);
    }

    double getWidth() {
        return Math.min(sideA, sideB);
    }
}
