package ru.cft.focusstart.task2.reader;

public class ShapeReaderException extends RuntimeException {

    ShapeReaderException(String message) {
        super(message);
    }
}
