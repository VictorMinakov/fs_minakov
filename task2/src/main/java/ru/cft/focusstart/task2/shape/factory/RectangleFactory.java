package ru.cft.focusstart.task2.shape.factory;

import ru.cft.focusstart.task2.shape.Rectangle;
import ru.cft.focusstart.task2.shape.Shape;

public class RectangleFactory extends ShapeFactory {

    private static final int COUNT_RECTANGLE_PARAMS = 2;

    public RectangleFactory(double[] shapeParams) {
        super(shapeParams);
        countParams = COUNT_RECTANGLE_PARAMS;
        validateData();
    }

    @Override
    public Shape createShape() {
        return new Rectangle(shapeParams[0], shapeParams[1]);
    }

    @Override
    void validateData() {
        super.validateData();
        validateRectangle(shapeParams[0], shapeParams[1]);
    }

    private void validateRectangle(double sideA, double sideB) {
        if (sideA <= 0) {
            throw new IllegalArgumentException("SideA must be > 0");
        }
        if (sideB <= 0) {
            throw new IllegalArgumentException("SideB must be > 0");
        }
    }
}
