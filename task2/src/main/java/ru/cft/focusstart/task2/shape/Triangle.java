package ru.cft.focusstart.task2.shape;

public class Triangle extends Shape {

    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    @Override
    protected String getName() {
        return "треугольник";
    }

    @Override
    public double getArea() {
        double semiperimeter = (sideA + sideB + sideC) / 2.0;
        return Math.sqrt(semiperimeter * (semiperimeter - sideA) * (semiperimeter - sideB) * (semiperimeter - sideC));
    }

    @Override
    public double getPerimeter() {
        return sideA + sideB + sideC;
    }

    @Override
    public String getStringInformation() {
        return super.getStringInformation() +
                getOppositeAngleInformation("A", sideA, sideB, sideC) + "\n" +
                getOppositeAngleInformation("B", sideB, sideA, sideC) + "\n" +
                getOppositeAngleInformation("C", sideC, sideA, sideB);
    }

    double getOppositeAngle(double currentSide, double otherSideA, double otherSideB) {
        return Math.toDegrees(Math.acos((otherSideA * otherSideA + otherSideB * otherSideB - currentSide * currentSide)
                / (2.0 * otherSideA * otherSideB)));
    }

    private String getOppositeAngleInformation(String sideName, double currentSide,
                                               double otherSideA, double otherSideB) {
        return "Длина стороны " + sideName + ":" + currentSide +
                ", противолежащий угол: " + getOppositeAngle(currentSide, otherSideA, otherSideB);
    }
}
