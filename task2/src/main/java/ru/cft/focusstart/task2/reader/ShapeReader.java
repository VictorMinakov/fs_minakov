package ru.cft.focusstart.task2.reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focusstart.task2.shape.Shape;
import ru.cft.focusstart.task2.shape.factory.CircleFactory;
import ru.cft.focusstart.task2.shape.factory.RectangleFactory;
import ru.cft.focusstart.task2.shape.factory.ShapeFactory;
import ru.cft.focusstart.task2.shape.factory.TriangleFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

public class ShapeReader {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShapeReader.class);

    private ShapeReader() {

    }

    public static Shape readShape(File file, Charset charset) {
        try {
            List<String> text = Files.readAllLines(file.toPath(), charset);
            text.removeIf(String::isEmpty);
            if (text.size() != 2) {
                throw new ShapeReaderException("File should contain 2 not-zero lines");
            }
            return parseShape(text);
        } catch (IOException e) {
            LOGGER.error("Error witch reading file " + file.getAbsolutePath(), e);
        }
        return null;
    }

    private static Shape parseShape(List<String> text) {
        double[] shapeParams;
        try {
            shapeParams = Arrays.stream(text.get(1).split(" "))
                    .mapToDouble(Double::valueOf)
                    .toArray();
        } catch (NumberFormatException e) {
            throw new ShapeReaderException("Error reading shape arguments. Arguments must be represented as numbers");
        }
        String shapeName = text.get(0).toUpperCase();
        return createShape(shapeName, shapeParams);
    }


    private static Shape createShape(String shapeName, double[] shapeParams) {
        try {
            ShapeFactory shapeFactory;
            switch (shapeName) {
                case "CIRCLE":
                    shapeFactory = new CircleFactory(shapeParams);
                    break;
                case "RECTANGLE":
                    shapeFactory = new RectangleFactory(shapeParams);
                    break;
                case "TRIANGLE":
                    shapeFactory = new TriangleFactory(shapeParams);
                    break;
                default:
                    throw new ShapeReaderException("Incorrect shape name");
            }
            return shapeFactory.createShape();
        } catch (IllegalArgumentException e) {
            throw new ShapeReaderException("Error creating shape: " + e.getMessage());
        }
    }
}
