package ru.cft.focusstart.task2.shape;

public class Circle extends Shape {

    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    protected String getName() {
        return "круг";
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return 2.0 * Math.PI * radius;
    }

    @Override
    public String getStringInformation() {
        return super.getStringInformation() +
                "Радиус: " + radius + "\n" +
                "Диаметр: " + getDiameter();
    }

    double getDiameter() {
        return 2.0 * radius;
    }
}
