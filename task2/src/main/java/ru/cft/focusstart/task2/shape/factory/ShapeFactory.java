package ru.cft.focusstart.task2.shape.factory;

import ru.cft.focusstart.task2.shape.Shape;

public abstract class ShapeFactory {

    double[] shapeParams;
    int countParams;

    ShapeFactory(double[] shapeParams) {
        this.shapeParams = shapeParams;
    }

    public abstract Shape createShape();

    void validateData() {
        if (shapeParams.length != countParams) {
            throw new IllegalArgumentException("Wrong number of shape parameters");
        }
    }
}
