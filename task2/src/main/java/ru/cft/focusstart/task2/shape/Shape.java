package ru.cft.focusstart.task2.shape;

public abstract class Shape {

    public abstract double getArea();

    public abstract double getPerimeter();

    public String getStringInformation() {
        return "Фигура: " + getName() + "\n" +
                "Площадь: " + getArea() + "\n" +
                "Периметр: " + getPerimeter() + "\n";
    }

    protected String getName() {
        return this.getClass().getSimpleName();
    }
}
