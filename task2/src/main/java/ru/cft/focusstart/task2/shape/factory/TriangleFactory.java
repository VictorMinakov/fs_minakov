package ru.cft.focusstart.task2.shape.factory;

import ru.cft.focusstart.task2.shape.Shape;
import ru.cft.focusstart.task2.shape.Triangle;

public class TriangleFactory extends ShapeFactory {

    private static final int COUNT_TRIANGLE_PARAMS = 3;

    public TriangleFactory(double[] shapeParams) {
        super(shapeParams);
        countParams = COUNT_TRIANGLE_PARAMS;
        validateData();
    }

    @Override
    public Shape createShape() {
        return new Triangle(shapeParams[0], shapeParams[1], shapeParams[2]);
    }

    @Override
    void validateData() {
        super.validateData();
        validateTriangle(shapeParams[0], shapeParams[1], shapeParams[2]);
    }

    private void validateTriangle(double sideA, double sideB, double sideC) {
        if (sideA <= 0) {
            throw new IllegalArgumentException("SideA must be > 0");
        }
        if (sideB <= 0) {
            throw new IllegalArgumentException("SideB must be > 0");
        }
        if (sideC <= 0) {
            throw new IllegalArgumentException("SideC must be > 0");
        }
        if ((sideA + sideB <= sideC) || (sideA + sideC <= sideB) || (sideB + sideC <= sideA)) {
            String rule = "the length of a side of a triangle is less than the sum of the lengths of the other" +
                    " two sides and greater than the difference of the lengths of the other two sides";
            throw new IllegalArgumentException(rule);
        }
    }
}
