package ru.cft.focusstart.task2.parser;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.internal.Nullable;

import java.io.File;

public class ArgumentParser {

    private static final String ARGUMENT_OUTPUT_FILE = "-output";
    private static final String ARGUMENT_INPUT_FILE = "-input";
    private static final String ARGUMENT_HELP = "-help";

    @Parameter(names = ARGUMENT_HELP, help = true)
    private boolean usedHelpArgument = false;

    @Parameter(names = {ARGUMENT_INPUT_FILE}, required = true)
    private String inputFileParameter = null;

    @Nullable
    @Parameter(names = {ARGUMENT_OUTPUT_FILE})
    private String outputFileParameter = null;

    private String[] commandLineArgs;

    public ArgumentParser(String[] commandLineArgs) {
        this.commandLineArgs = commandLineArgs;
        parseArguments();
    }

    public File getInputFile() {
        return new File(inputFileParameter);
    }

    public File getOutputFile() {
        if (outputFileParameter != null) {
            return new File(outputFileParameter);
        }
        return null;
    }

    private void parseArguments() {
        JCommander.newBuilder()
                .addObject(this)
                .build()
                .parse(commandLineArgs);
        if (usedHelpArgument) {
            printHelp();
            System.exit(0);
        }
    }

    private void printHelp() {
        String help = "Для запуска программы задайте следующие аргументы:\n" +
                "1.Входной файл с описанием фигуры: -input\n" +
                "2.Выходной файл (если используется вывод в файл): -output\n" +
                "Пример (c выводом в файл): program -input input.txt -output output.txt\n" +
                "Пример (с выводом в консоль): program -input input.txt";
        System.out.println(help);
    }
}
