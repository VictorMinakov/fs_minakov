package ru.cft.focusstart.task2.writer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focusstart.task2.shape.Shape;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;

public class ShapeWriter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ShapeWriter.class);

    private ShapeWriter() {

    }

    public static void writeShape(Shape shape, File file, Charset charset) {
        try (BufferedWriter writer = Files.newBufferedWriter(file.toPath(), charset)) {
            writer.write(shape.getStringInformation());
        } catch (IOException e) {
            LOGGER.error("Error witch writing file " + file.getAbsolutePath(), e);
        }
    }

    public static void writeShape(Shape shape, PrintStream printStream) {
        printStream.println(shape.getStringInformation());
    }
}
