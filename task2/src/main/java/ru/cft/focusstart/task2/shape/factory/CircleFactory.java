package ru.cft.focusstart.task2.shape.factory;

import ru.cft.focusstart.task2.shape.Circle;
import ru.cft.focusstart.task2.shape.Shape;

public class CircleFactory extends ShapeFactory {

    private static final int COUNT_CIRCLE_PARAMS = 1;

    public CircleFactory(double[] shapeParams) {
        super(shapeParams);
        countParams = COUNT_CIRCLE_PARAMS;
        validateData();
    }

    @Override
    public Shape createShape() {
        return new Circle(shapeParams[0]);
    }

    @Override
    void validateData() {
        super.validateData();
        validateCircle(shapeParams[0]);
    }

    private void validateCircle(double radius) {
        if (radius <= 0) {
            throw new IllegalArgumentException("Radius must be > 0");
        }
    }
}
