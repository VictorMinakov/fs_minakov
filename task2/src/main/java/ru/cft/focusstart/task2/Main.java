package ru.cft.focusstart.task2;

import com.beust.jcommander.ParameterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focusstart.task2.parser.ArgumentParser;
import ru.cft.focusstart.task2.reader.ShapeReader;
import ru.cft.focusstart.task2.reader.ShapeReaderException;
import ru.cft.focusstart.task2.shape.Shape;
import ru.cft.focusstart.task2.writer.ShapeWriter;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    private static final Charset CHARSET = StandardCharsets.UTF_8;

    public static void main(String[] args) {
        try {
            ArgumentParser argumentParser = new ArgumentParser(args);
            File inputFile = argumentParser.getInputFile();
            File outputFile = argumentParser.getOutputFile();
            Shape shape = ShapeReader.readShape(inputFile, CHARSET);
            if (shape != null) {
                if (outputFile != null) {
                    ShapeWriter.writeShape(shape, outputFile, CHARSET);
                } else {
                    ShapeWriter.writeShape(shape, System.out);
                }
            }
        } catch (ParameterException e) {
            LOGGER.error("Incorrect command line arguments", e);
        } catch (ShapeReaderException e) {
            String message = "Incorrect data from input file. " + e.getMessage();
            LOGGER.error(message);
        }
    }
}
