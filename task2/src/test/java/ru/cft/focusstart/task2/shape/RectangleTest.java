package ru.cft.focusstart.task2.shape;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RectangleTest {

    private static final float DELTA = 0.0001f;

    @ParameterizedTest
    @CsvSource({"5, 10, 50", "3, 5, 15"})
    void testArea(double sideA, double sideB, double actualResult) {
        assertEquals(new Rectangle(sideA, sideB).getArea(), actualResult, DELTA);
    }

    @ParameterizedTest
    @CsvSource({"3, 10, 26", "3, 15, 36"})
    void testPerimeter(double sideA, double sideB, double actualResult) {
        assertEquals(new Rectangle(sideA, sideB).getPerimeter(), actualResult, DELTA);
    }

    @ParameterizedTest
    @CsvSource({"3, 10, 10.4403", "3, 15, 15.297"})
    void testDiagonal(double sideA, double sideB, double actualResult) {
        assertEquals(new Rectangle(sideA, sideB).getDiagonal(), actualResult, DELTA);
    }

    @ParameterizedTest
    @CsvSource({"3, 10, 10", "3, 15, 15"})
    void testLength(double sideA, double sideB, double actualResult) {
        assertEquals(new Rectangle(sideA, sideB).getLength(), actualResult, DELTA);
    }

    @ParameterizedTest
    @CsvSource({"3, 10, 3", "15, 2, 2"})
    void testWidth(double sideA, double sideB, double actualResult) {
        assertEquals(new Rectangle(sideA, sideB).getWidth(), actualResult, DELTA);
    }
}
