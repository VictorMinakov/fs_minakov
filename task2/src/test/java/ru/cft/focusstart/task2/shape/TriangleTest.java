package ru.cft.focusstart.task2.shape;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TriangleTest {

    private static final float DELTA = 0.0001f;

    @ParameterizedTest
    @CsvSource({"2, 3, 4, 2.9047", "4, 5, 8, 8.1815"})
    void testArea(double sideA, double sideB, double sideC, double actualResult) {
        assertEquals(new Triangle(sideA, sideB, sideC).getArea(), actualResult, DELTA);
    }

    @ParameterizedTest
    @CsvSource({"2, 3, 4, 9", "4, 5, 8, 17"})
    void testPerimeter(double sideA, double sideB, double sideC, double actualResult) {
        assertEquals(new Triangle(sideA, sideB, sideC).getPerimeter(), actualResult, DELTA);
    }

    @ParameterizedTest
    @CsvSource({"2, 3, 4, 2, 3, 4, 28.955", "2, 3, 4, 3, 2, 4, 46.5674", "2, 3, 4, 4, 2, 3, 104.4775"})
    void testOppositeAngle(double sideA, double sideB, double sideC, double currentSide,
                           double otherSideA, double otherSideB, double actualResult) {
        assertEquals(new Triangle(sideA, sideB, sideC)
                .getOppositeAngle(currentSide, otherSideA, otherSideB), actualResult, DELTA);
    }
}