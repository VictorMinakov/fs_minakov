package ru.cft.focusstart.task2.shape.factory;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertThrows;

class CircleFactoryTest {

    @ParameterizedTest
    @CsvSource({"-5", "-0.5"})
    void testIncorrectCircle(double radius) {
        assertThrows(IllegalArgumentException.class, () -> new CircleFactory(new double[] {radius}).createShape());
    }
}