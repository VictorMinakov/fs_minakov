package ru.cft.focusstart.task2.shape.factory;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertThrows;

class RectangleFactoryTest {

    @ParameterizedTest
    @CsvSource({"-5, 5", "5, -5", "-10, -10"})
    void testIncorrectRectangle(double sideA, double sideB) {
        assertThrows(IllegalArgumentException.class,
                () -> new CircleFactory(new double[] {sideA, sideB}).createShape());
    }
}