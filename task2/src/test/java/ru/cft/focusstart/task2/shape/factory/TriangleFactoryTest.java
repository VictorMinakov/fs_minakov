package ru.cft.focusstart.task2.shape.factory;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertThrows;

class TriangleFactoryTest {

    @ParameterizedTest
    @CsvSource({"-5, 5, 10", "5, -5, 10", "5, 5, -10", "2, 3, 7", "1, 7, 23"})
    void testIncorrectTriangle(double sideA, double sideB, double sideC) {
        assertThrows(IllegalArgumentException.class,
                () -> new CircleFactory(new double[]{sideA, sideB, sideC}).createShape());
    }
}