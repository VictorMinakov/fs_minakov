package ru.cft.focusstart.task2.shape;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CircleTest {

    private static final float DELTA = 0.0001f;

    @ParameterizedTest
    @CsvSource({"5, 78.5398", "15, 706.8583"})
    void testArea(double radius, double actualResult) {
        assertEquals(new Circle(radius).getArea(), actualResult, DELTA);
    }

    @ParameterizedTest
    @CsvSource({"5, 31.4159", "15, 94.2477"})
    void testPerimeter(double radius, double actualResult) {
        assertEquals(new Circle(radius).getPerimeter(), actualResult, DELTA);
    }

    @ParameterizedTest
    @CsvSource({"5, 10", "15, 30"})
    void testDiameter(double radius, double actualResult) {
        assertEquals(new Circle(radius).getDiameter(), actualResult, 0.0001);
    }
}