package ru.cft.focusstart.task3.model;

import java.util.stream.Stream;

public enum CellState {

    EMPTY(null),
    MINE(null),
    DIGIT1(1),
    DIGIT2(2),
    DIGIT3(3),
    DIGIT4(4),
    DIGIT5(5),
    DIGIT6(6),
    DIGIT7(7),
    DIGIT8(8);

    private Integer digitValue;

    CellState(Integer value) {
        digitValue = value;
    }

    public static boolean isDigit(CellState cellState) {
        return cellState != CellState.MINE && cellState != CellState.EMPTY;
    }

    public static CellState valueOfDigitValue(int value) {
        return Stream.of(CellState.values())
                .filter(e -> e.digitValue != null && e.digitValue.equals(value))
                .findAny()
                .orElseThrow(IllegalArgumentException::new);
    }

    public Integer getDigitValue() {
        return digitValue;
    }
}
