package ru.cft.focusstart.task3;

import ru.cft.focusstart.task3.controller.Controller;
import ru.cft.focusstart.task3.model.Difficulty;
import ru.cft.focusstart.task3.model.Field;
import ru.cft.focusstart.task3.model.scores.MinesweeperScoreManager;
import ru.cft.focusstart.task3.model.scores.ScoreManager;
import ru.cft.focusstart.task3.view.CustomDifficultyView;
import ru.cft.focusstart.task3.view.FieldView;
import ru.cft.focusstart.task3.view.NewPlayerNameView;
import ru.cft.focusstart.task3.view.ScoresView;

public class Minesweeper {

    public static void main(String[] args) {
        Difficulty startDifficulty = Difficulty.BEGINNER;
        Field field = new Field();
        FieldView fieldView = new FieldView();
        Controller controller = new Controller(field);

        CustomDifficultyView customDifficultyView = new CustomDifficultyView(fieldView.getFrame(), controller);
        ScoresView scoresView = new ScoresView(fieldView.getFrame());
        NewPlayerNameView newPlayerNameView = new NewPlayerNameView(fieldView.getFrame());

        fieldView.setCustomDifficultyView(customDifficultyView);
        fieldView.setScoresView(scoresView);
        fieldView.setNewPlayerNameView(newPlayerNameView);
        fieldView.setControllerListener(controller);

        ScoreManager scoreManager = new MinesweeperScoreManager();

        field.setScoreManager(scoreManager);
        field.registerObserver(fieldView);
        field.changeDifficulty(startDifficulty);
    }
}
