package ru.cft.focusstart.task3.model.observer;

import ru.cft.focusstart.task3.model.CellState;
import ru.cft.focusstart.task3.model.EndGameStatus;
import ru.cft.focusstart.task3.model.scores.Score;
import ru.cft.focusstart.task3.util.Point;

import java.util.List;
import java.util.Map;

public interface MinesweeperObserver {

    void updateCells(Map<Point, CellState> cells);

    void updateFlagOnCell(Point coordinate, boolean flagState);

    void updateTimer(int countSeconds);

    void updateCountNonSelectedMines(int countNonSelectedMines);

    void updateEndGameStatus(EndGameStatus status, Point currentPosition, List<Point> mines);

    void startTimer();

    void stopTimer();

    void updateScores(List<Score> scores);

    void updateScores(List<Score> scores, int listIndexNewRecord);

    void createNewGame(int rows, int columns);
}
