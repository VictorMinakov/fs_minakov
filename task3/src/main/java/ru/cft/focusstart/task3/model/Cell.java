package ru.cft.focusstart.task3.model;

class Cell {

    private boolean isOpened = false;
    private boolean isFlagInstalled = false;
    private CellState state;

    Cell(CellState state) {
        this.state = state;
    }

    boolean isOpened() {
        return isOpened;
    }

    void open() {
        isOpened = true;
    }

    boolean isFlagInstalled() {
        return isFlagInstalled;
    }

    void setFlagInstalled(boolean flagInstalled) {
        isFlagInstalled = flagInstalled;
    }

    CellState getState() {
        return state;
    }

    void setState(CellState state) {
        this.state = state;
    }
}
