package ru.cft.focusstart.task3.controller.timer;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class MinesweeperTimer {

    private Timer timer;
    private TimerUpdateListener timerUpdateListener;
    private long startTime;

    public MinesweeperTimer(TimerUpdateListener timerUpdateListener) {
        this.timerUpdateListener = timerUpdateListener;
    }

    public void start() {
        timer = new Timer();
        startTime = System.nanoTime();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                int countSeconds = (int) TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - startTime);
                timerUpdateListener.updateTimer(countSeconds);
            }
        }, 0, TimeUnit.SECONDS.toMillis(1));
    }

    public void stop() {
        if (timer != null) {
            timer.cancel();
        }
    }
}
