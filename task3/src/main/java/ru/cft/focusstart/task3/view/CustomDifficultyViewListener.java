package ru.cft.focusstart.task3.view;

import ru.cft.focusstart.task3.model.Difficulty;

public interface CustomDifficultyViewListener {

    void changeDifficulty(Difficulty difficulty);
}
