package ru.cft.focusstart.task3.view;

import ru.cft.focusstart.task3.model.Difficulty;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ru.cft.focusstart.task3.view.LanguageProperties.getProperty;

public class CustomDifficultyView {

    private static final int WINDOW_HEIGHT = 200;
    private static final int WINDOW_WIDTH = 200;

    private static final int MINES_SPINNER_MIN = 10;
    private static final int MINES_SPINNER_MAX = 667;
    private static final int MINES_SPINNER_DEFAULT = 10;

    private static final int WIDTH_SPINNER_MIN = 9;
    private static final int WIDTH_SPINNER_MAX = 24;
    private static final int WIDTH_SPINNER_DEFAULT = 9;

    private static final int HEIGHT_SPINNER_MIN = 9;
    private static final int HEIGHT_SPINNER_MAX = 30;
    private static final int HEIGHT_SPINNER_DEFAULT = 9;

    private List<JLabel> labels;
    private List<JSpinner> spinners;
    private JSpinner heightSpinner;
    private JSpinner widthSpinner;
    private JSpinner minesSpinner;
    private JDialog dialog;
    private JPanel mainPanel;
    private CustomDifficultyViewListener customDifficultyViewListener;

    public CustomDifficultyView(JFrame parent, CustomDifficultyViewListener customDifficultyViewListener) {
        this.customDifficultyViewListener = customDifficultyViewListener;
        createDialog(parent);
        createLabels();
        createSpinners();
        createElements();
        dialog.pack();
    }

    void show() {
        dialog.setVisible(true);
    }

    private void createSpinners() {
        heightSpinner = new JSpinner(new SpinnerNumberModel(
                HEIGHT_SPINNER_DEFAULT, HEIGHT_SPINNER_MIN, HEIGHT_SPINNER_MAX, 1));
        widthSpinner = new JSpinner(new SpinnerNumberModel(
                WIDTH_SPINNER_DEFAULT, WIDTH_SPINNER_MIN, WIDTH_SPINNER_MAX, 1));
        minesSpinner = new JSpinner(new SpinnerNumberModel(
                MINES_SPINNER_DEFAULT, MINES_SPINNER_MIN, MINES_SPINNER_MAX, 1));
        heightSpinner.addChangeListener(e -> setupMineSpinner());
        widthSpinner.addChangeListener(e -> setupMineSpinner());
        spinners = Stream.of(heightSpinner, widthSpinner, minesSpinner).collect(Collectors.toList());
    }

    private void setupMineSpinner() {
        int rowsCount = (int) heightSpinner.getValue();
        int columnsCount = (int) widthSpinner.getValue();
        int maxMines = (rowsCount - 1) * (columnsCount - 1);
        int currentMinesCount = Math.min((int) minesSpinner.getValue(), maxMines);
        minesSpinner.setModel(new SpinnerNumberModel(currentMinesCount,
                MINES_SPINNER_MIN, maxMines, 1));
    }

    private void createElements() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(4, 2));
        for (int i = 0; i < 3; ++i) {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout());
            panel.add(labels.get(i));
            panel.add(spinners.get(i));
            mainPanel.add(panel);
        }
        dialog.add(mainPanel);
        createOKButton();
    }

    private void createLabels() {
        labels = new ArrayList<>(3);
        labels.add(new JLabel(getProperty("custom_difficulty_view.label_height")));
        labels.add(new JLabel(getProperty("custom_difficulty_view.label_width")));
        labels.add(new JLabel(getProperty("custom_difficulty_view.label_mines")));
    }

    private void createOKButton() {
        JButton okButton = new JButton(getProperty("custom_difficulty_view.button_ok"));
        okButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int rowsCount = (int) heightSpinner.getValue();
                int columnsCount = (int) widthSpinner.getValue();
                int minesCount = (int) minesSpinner.getValue();
                customDifficultyViewListener.changeDifficulty(new Difficulty(rowsCount, columnsCount, minesCount));
                dialog.setVisible(false);
            }
        });
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        mainPanel.add(buttonPanel);
    }

    private void createDialog(JFrame parent) {
        dialog = new JDialog(parent);
        dialog.setModal(true);
        dialog.setTitle(getProperty("custom_difficulty_view.title"));
        dialog.setResizable(false);
        dialog.setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        dialog.setLocationRelativeTo(parent);
    }
}
