package ru.cft.focusstart.task3.model;

public enum EndGameStatus {

    WIN,
    LOSE
}
