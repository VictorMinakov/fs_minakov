package ru.cft.focusstart.task3.view;

import ru.cft.focusstart.task3.controller.ControllerListener;
import ru.cft.focusstart.task3.model.scores.Score;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static ru.cft.focusstart.task3.view.LanguageProperties.getProperty;

public class ScoresView {

    private static final int WINDOW_HEIGHT = 200;
    private static final int WINDOW_WIDTH = 300;

    private List<JLabel> labelsDifficulty;
    private List<JLabel> labelsScores;
    private List<JLabel> labelsUsers;
    private List<Score> scores;
    private JDialog dialog;
    private JPanel mainPanel;

    private ControllerListener controllerListener;

    public ScoresView(JFrame parent) {
        createDialog(parent);
        createLabels();
        createElements();
        createButtons();
        dialog.pack();
    }

    void setControllerListener(ControllerListener controllerListener) {
        this.controllerListener = controllerListener;
    }

    void setScores(List<Score> scores) {
        replaceNullNamesToDefault(scores);
        this.scores = scores;
        updateLabels();
    }

    void show() {
        dialog.setVisible(true);
    }

    private void replaceNullNamesToDefault(List<Score> scores) {
        ListIterator<Score> iterator = scores.listIterator();
        while (iterator.hasNext()) {
            Score score = iterator.next();
            if (score.getPlayerName() == null) {
                iterator.remove();
                iterator.add(new Score(score.getDifficulty(), score.getTime(), getProperty("scores_view.default_user")));
            }
        }
    }

    private void createDialog(JFrame parent) {
        dialog = new JDialog(parent);
        dialog.setModal(true);
        dialog.setTitle(getProperty("scores_view.title"));
        dialog.setResizable(false);
        dialog.setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        dialog.setLocationRelativeTo(null);
    }

    private void createLabels() {
        labelsDifficulty = new ArrayList<>(3);
        labelsDifficulty.add(new JLabel(getProperty("scores_view.label_difficulty_beginner")));
        labelsDifficulty.add(new JLabel(getProperty("scores_view.label_difficulty_intermediate")));
        labelsDifficulty.add(new JLabel(getProperty("scores_view.label_difficulty_expert")));

        labelsScores = new ArrayList<>(3);
        labelsUsers = new ArrayList<>(3);
        for (int i = 0; i < 3; ++i) {
            labelsScores.add(new JLabel());
            labelsUsers.add(new JLabel());
        }
    }

    private void createButtons() {
        JButton okButton = new JButton(getProperty("scores_view.button_ok"));
        okButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dialog.setVisible(false);
            }
        });
        JButton resetButton = new JButton(getProperty("scores_view.button_reset"));
        okButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                dialog.setVisible(false);
            }
        });
        resetButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                controllerListener.clearScores();
            }
        });
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(resetButton);
        buttonPanel.add(okButton);
        mainPanel.add(buttonPanel);
    }

    private void createElements() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(4, 3));
        for (int i = 0; i < 3; ++i) {
            JPanel panel = new JPanel();
            panel.setLayout(new FlowLayout());
            panel.add(labelsDifficulty.get(i));
            panel.add(labelsScores.get(i));
            panel.add(labelsUsers.get(i));
            mainPanel.add(panel);
        }
        dialog.add(mainPanel);
    }

    private void updateLabels() {
        for (int i = 0; i < scores.size(); ++i) {
            labelsScores.get(i).setText(scores.get(i).getTime() + " " + getProperty("scores_view.seconds"));
            labelsUsers.get(i).setText(String.valueOf(scores.get(i).getPlayerName()));
        }
    }
}
