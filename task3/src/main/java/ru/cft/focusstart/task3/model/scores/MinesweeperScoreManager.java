package ru.cft.focusstart.task3.model.scores;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focusstart.task3.model.Difficulty;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class MinesweeperScoreManager implements ScoreManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(MinesweeperScoreManager.class);
    private static final File SCORES_FILE = new File("scores.json");
    private static final int SCORES_DEFAULT_TIME = 999;

    @Override
    public List<Score> read() {
        List<Score> scores;
        Gson gson = new Gson();
        try (JsonReader reader = new JsonReader(new FileReader(SCORES_FILE))) {
            scores = gson.fromJson(reader, new TypeToken<List<Score>>() {
            }.getType());
        } catch (IOException ignored) {
            LOGGER.warn("Error witch reading scores file {}. Generated default scores", SCORES_FILE.getAbsolutePath());
            scores = createDefaultScores();
        }
        return scores;
    }

    @Override
    public void write(List<Score> scores) {
        try (Writer writer = new FileWriter(SCORES_FILE)) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            gson.toJson(scores, writer);
        } catch (IOException e) {
            LOGGER.error("Error witch writing scores to file " + SCORES_FILE.getAbsolutePath(), e);
        }
    }

    @Override
    public void clearScores() {
        try {
            Files.delete(SCORES_FILE.toPath());
        } catch (IOException e) {
            LOGGER.error("Error witch removing scores file " + SCORES_FILE.getAbsolutePath(), e);
        }
    }

    private List<Score> createDefaultScores() {
        List<Score> scores = new ArrayList<>(3);
        scores.add(new Score(Difficulty.BEGINNER, SCORES_DEFAULT_TIME, null));
        scores.add(new Score(Difficulty.INTERMEDIATE, SCORES_DEFAULT_TIME, null));
        scores.add(new Score(Difficulty.EXPERT, SCORES_DEFAULT_TIME, null));
        return scores;
    }
}
