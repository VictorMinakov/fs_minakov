package ru.cft.focusstart.task3.controller.timer;

public interface TimerUpdateListener {

    void updateTimer(int countSeconds);
}
