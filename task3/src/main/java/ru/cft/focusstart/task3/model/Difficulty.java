package ru.cft.focusstart.task3.model;

public class Difficulty {

    public static final Difficulty BEGINNER = new Difficulty(9, 9, 10);
    public static final Difficulty INTERMEDIATE = new Difficulty(16, 16, 40);
    public static final Difficulty EXPERT = new Difficulty(16, 30, 99);

    private int countRows;
    private int countColumns;
    private int countMines;

    public Difficulty(int countRows, int countColumns, int countMines) {
        this.countRows = countRows;
        this.countColumns = countColumns;
        this.countMines = countMines;
    }

    int getCountRows() {
        return countRows;
    }

    int getCountColumns() {
        return countColumns;
    }

    int getCountMines() {
        return countMines;
    }
}
