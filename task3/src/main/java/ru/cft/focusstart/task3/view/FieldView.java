package ru.cft.focusstart.task3.view;

import ru.cft.focusstart.task3.controller.ControllerListener;
import ru.cft.focusstart.task3.model.CellState;
import ru.cft.focusstart.task3.model.Difficulty;
import ru.cft.focusstart.task3.model.EndGameStatus;
import ru.cft.focusstart.task3.model.observer.MinesweeperObserver;
import ru.cft.focusstart.task3.model.scores.Score;
import ru.cft.focusstart.task3.util.Point;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.cft.focusstart.task3.view.LanguageProperties.getProperty;

public class FieldView implements MinesweeperObserver {

    private static final int CELL_SIZE = 32;
    private static final int NEW_GAME_BUTTON_SIZE = 64;
    private static final int BORDER_SIZE = 12;

    private CustomDifficultyView customDifficultyView;
    private ScoresView scoresView;
    private NewPlayerNameView newPlayerNameView;

    private JFrame frame;
    private JPanel mainPanel;
    private JPanel buttonPanel;
    private JButton newGameButton;
    private JButton[][] buttons;
    private JLabel[] minesCount;
    private JLabel[] timer;

    private int rows;
    private int columns;

    private ControllerListener controllerListener;

    public FieldView() {
        Textures.loadTextures();
        createFrame();
        createMenu();
        createNewGameButton();
        createTopLayout();
    }

    @Override
    public void updateCells(Map<Point, CellState> cells) {
        cells.keySet().forEach(cell -> {
            ImageIcon icon = Textures.getCellIcon(cells.get(cell));
            buttons[cell.y][cell.x].setDisabledIcon(icon);
            buttons[cell.y][cell.x].setEnabled(false);
        });
    }

    @Override
    public void updateFlagOnCell(Point coordinate, boolean flagState) {
        if (flagState) {
            buttons[coordinate.y][coordinate.x].setIcon(Textures.ICON_FLAG);
        } else {
            buttons[coordinate.y][coordinate.x].setIcon(Textures.ICON_BUTTON);
        }
    }

    @Override
    public void updateTimer(int countSeconds) {
        if (countSeconds < 999) {
            setRedDigits(timer, countSeconds);
        }
    }

    @Override
    public void updateCountNonSelectedMines(int countNonSelectedMines) {
        if (countNonSelectedMines > -100) {
            setRedDigits(minesCount, countNonSelectedMines);
        }
    }

    @Override
    public void updateEndGameStatus(EndGameStatus status, Point currentPosition, List<Point> mines) {
        Map<Point, CellState> cells = new HashMap<>(mines.size());
        for (Point mineCoordinate : mines) {
            cells.put(mineCoordinate, CellState.MINE);
        }
        updateCells(cells);
        if (status == EndGameStatus.WIN) {
            newGameButton.setIcon(Textures.ICON_SMILE_WIN);
            clearRedDigitLabels(minesCount);
        } else if (status == EndGameStatus.LOSE) {
            buttons[currentPosition.y][currentPosition.x].setDisabledIcon(Textures.ICON_MINE_RED);
            newGameButton.setIcon(Textures.ICON_SMILE_LOSE);
        }
        setIconToDisableIconOnButtons();
    }

    @Override
    public void startTimer() {
        controllerListener.startTimer();
    }

    @Override
    public void stopTimer() {
        controllerListener.stopTimer();
    }

    @Override
    public void updateScores(List<Score> scores) {
        scoresView.setScores(scores);
    }

    @Override
    public void updateScores(List<Score> scores, int listIndexNewRecord) {
        newPlayerNameView.show();
        String playerName = newPlayerNameView.getPlayerName();
        Score scoreWithoutPlayerName = scores.get(listIndexNewRecord);
        Score scoreWithPlayerName = new Score(scoreWithoutPlayerName.getDifficulty(),
                scoreWithoutPlayerName.getTime(), playerName);
        scores.set(listIndexNewRecord, scoreWithPlayerName);
        updateScores(scores);
        controllerListener.writeScoresToFile(scores);
        scoresView.show();
    }

    @Override
    public void createNewGame(int rows, int columns) {
        createField(rows, columns);
    }

    public void setControllerListener(ControllerListener controllerListener) {
        this.controllerListener = controllerListener;
        scoresView.setControllerListener(controllerListener);
    }

    public void setCustomDifficultyView(CustomDifficultyView customDifficultyView) {
        this.customDifficultyView = customDifficultyView;
    }

    public void setScoresView(ScoresView scoresView) {
        this.scoresView = scoresView;
    }

    public void setNewPlayerNameView(NewPlayerNameView newPlayerNameView) {
        this.newPlayerNameView = newPlayerNameView;
    }

    public JFrame getFrame() {
        return frame;
    }

    private void createFrame() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle(getProperty("main_view.title"));
        frame.setIconImage(Textures.ICON_WINDOW.getImage());
        frame.setResizable(false);
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        frame.add(mainPanel);
    }

    private void createMenu() {
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu(getProperty("main_view.menu_game"));
        JMenuItem menuItemNew = new JMenuItem(getProperty("main_view.menu_new"));
        menuItemNew.addActionListener(e -> controllerListener.createNewGame());
        JMenuItem menuItemBeginner = new JMenuItem(getProperty("main_view.menu_difficulty_beginner"));
        menuItemBeginner.addActionListener(e -> controllerListener.changeDifficulty(Difficulty.BEGINNER));
        JMenuItem menuItemIntermediate = new JMenuItem(getProperty("main_view.menu_difficulty_intermediate"));
        menuItemIntermediate.addActionListener(e -> controllerListener.changeDifficulty(Difficulty.INTERMEDIATE));
        JMenuItem menuItemExpert = new JMenuItem(getProperty("main_view.menu_difficulty_expert"));
        menuItemExpert.addActionListener(e -> controllerListener.changeDifficulty(Difficulty.EXPERT));
        JMenuItem menuItemOther = new JMenuItem(getProperty("main_view.menu_difficulty_other"));
        menuItemOther.addActionListener(e -> customDifficultyView.show());
        JMenuItem menuItemScores = new JMenuItem(getProperty("main_view.menu_scores"));
        menuItemScores.addActionListener(e -> {
            controllerListener.updateScores();
            scoresView.show();
        });
        JMenuItem menuItemExit = new JMenuItem(getProperty("main_view.menu_exit"));
        menuItemExit.addActionListener(e -> frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING)));
        menu.add(menuItemNew);
        menu.addSeparator();
        menu.add(menuItemBeginner);
        menu.add(menuItemIntermediate);
        menu.add(menuItemExpert);
        menu.add(menuItemOther);
        menu.addSeparator();
        menu.add(menuItemScores);
        menu.addSeparator();
        menu.add(menuItemExit);
        menuBar.add(menu);
        frame.setJMenuBar(menuBar);
    }

    private void createNewGameButton() {
        newGameButton = new JButton();
        newGameButton.setIcon(Textures.ICON_SMILE_NORMAL);
        newGameButton.setPreferredSize(new Dimension(NEW_GAME_BUTTON_SIZE, NEW_GAME_BUTTON_SIZE));
        newGameButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                controllerListener.createNewGame();
            }
        });
    }

    private void createTopLayout() {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.setBorder(new CompoundBorder(new EmptyBorder(BORDER_SIZE, BORDER_SIZE,
                BORDER_SIZE, BORDER_SIZE), new BevelBorder(BevelBorder.LOWERED)));
        createMinesCountLabels(panel);
        panel.add(newGameButton);
        createTimerLabels(panel);
        mainPanel.add(panel, BorderLayout.NORTH);
    }

    private void createMinesCountLabels(JPanel panel) {
        minesCount = new JLabel[3];
        for (int i = 0; i < minesCount.length; ++i) {
            JLabel label = new JLabel();
            minesCount[i] = label;
            panel.add(label);
        }
        clearRedDigitLabels(minesCount);
    }

    private void createTimerLabels(JPanel panel) {
        timer = new JLabel[3];
        for (int i = 0; i < timer.length; ++i) {
            JLabel label = new JLabel();
            timer[i] = label;
            panel.add(label);
        }
        clearRedDigitLabels(timer);
    }

    private void clearRedDigitLabels(JLabel[] labels) {
        for (JLabel label : labels) {
            label.setIcon(Textures.getRedDigitIcon('0'));
        }
    }

    private void createButtons() {
        buttons = new JButton[rows][columns];
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < columns; ++j) {
                final Point cellCoordinate = new Point(j, i);
                buttons[i][j] = new JButton();
                buttons[i][j].setIcon(Textures.ICON_BUTTON);
                buttons[i][j].setPreferredSize(new Dimension(CELL_SIZE, CELL_SIZE));
                buttons[i][j].addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent e) {
                        controllerListener.cellClick(e.getButton(), cellCoordinate);
                    }
                });
            }
        }
    }

    private void drawButtons() {
        if (buttonPanel != null) {
            mainPanel.remove(buttonPanel);
        }
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(rows, columns));
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < columns; ++j) {
                buttonPanel.add(buttons[i][j]);
            }
        }
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);
    }

    private void createField(int rows, int columns) {
        controllerListener.stopTimer();
        clearRedDigitLabels(timer);
        newGameButton.setIcon(Textures.ICON_SMILE_NORMAL);
        this.rows = rows;
        this.columns = columns;
        createButtons();
        drawButtons();
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void setIconToDisableIconOnButtons() {
        for (JButton[] row : buttons) {
            for (JButton button : row) {
                if (button.isEnabled()) {
                    button.setDisabledIcon(button.getIcon());
                    button.setEnabled(false);
                }
            }
        }
    }

    private void setRedDigits(JLabel[] labelsArray, int value) {
        char[] digits = {'0', '0', '0'};
        char[] strValue = String.valueOf(value).toCharArray();
        System.arraycopy(strValue, 0, digits, digits.length - strValue.length, strValue.length);
        for (int i = 0; i < labelsArray.length; ++i) {
            labelsArray[i].setIcon(Textures.getRedDigitIcon(digits[i]));
        }
    }
}
