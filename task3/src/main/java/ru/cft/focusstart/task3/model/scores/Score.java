package ru.cft.focusstart.task3.model.scores;

import ru.cft.focusstart.task3.model.Difficulty;

public class Score {

    private Difficulty difficulty;
    private int time;
    private String playerName;

    public Score(Difficulty difficulty, int time, String playerName) {
        this.difficulty = difficulty;
        this.time = time;
        this.playerName = playerName;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public int getTime() {
        return time;
    }

    public String getPlayerName() {
        return playerName;
    }
}
