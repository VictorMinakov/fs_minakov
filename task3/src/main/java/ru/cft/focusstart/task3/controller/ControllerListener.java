package ru.cft.focusstart.task3.controller;

import ru.cft.focusstart.task3.model.Difficulty;
import ru.cft.focusstart.task3.model.scores.Score;
import ru.cft.focusstart.task3.util.Point;

import java.util.List;

public interface ControllerListener {

    void cellClick(int mouseButton, Point coordinate);

    void changeDifficulty(Difficulty difficulty);

    void createNewGame();

    void startTimer();

    void stopTimer();

    void updateScores();

    void writeScoresToFile(List<Score> scoreList);

    void clearScores();
}
