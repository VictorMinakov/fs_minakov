package ru.cft.focusstart.task3.view;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focusstart.task3.model.CellState;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.io.IOException;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

class Textures {

    static final ImageIcon ICON_WINDOW = new ImageIcon();
    static final ImageIcon ICON_BUTTON = new ImageIcon();
    static final ImageIcon ICON_FLAG = new ImageIcon();
    static final ImageIcon ICON_SMILE_NORMAL = new ImageIcon();
    static final ImageIcon ICON_SMILE_WIN = new ImageIcon();
    static final ImageIcon ICON_SMILE_LOSE = new ImageIcon();
    static final ImageIcon ICON_MINE_RED = new ImageIcon();

    private static final Logger LOGGER = LoggerFactory.getLogger(LanguageProperties.class);
    private static Map<CellState, ImageIcon> cellIcons;
    private static Map<Character, ImageIcon> redDigitIcons;

    private Textures() {

    }

    static ImageIcon getCellIcon(CellState cellState) {
        return cellIcons.get(cellState);
    }

    static ImageIcon getRedDigitIcon(char redDigit) {
        return redDigitIcons.get(redDigit);
    }

    static void loadTextures() {
        try {
            loadDigitTextures();
            loadRedDigitTextures();
            ICON_WINDOW.setImage(ImageIO.read(Textures.class.getResource("/icon.png")));
            ICON_BUTTON.setImage(ImageIO.read(Textures.class.getResource("/images/button.png")));
            ICON_FLAG.setImage(ImageIO.read(Textures.class.getResource("/images/flag.png")));
            ICON_SMILE_NORMAL.setImage(ImageIO.read(Textures.class.getResource("/images/smile/normal.png")));
            ICON_SMILE_WIN.setImage(ImageIO.read(Textures.class.getResource("/images/smile/win.png")));
            ICON_SMILE_LOSE.setImage(ImageIO.read(Textures.class.getResource("/images/smile/lose.png")));
            ICON_MINE_RED.setImage(ImageIO.read(Textures.class.getResource("/images/red_mine.png")));
        } catch (IOException e) {
            String message = "Error witch reading texture file";
            LOGGER.warn(message, e);
        }
    }

    private static void loadDigitTextures() throws IOException {
        cellIcons = new EnumMap<>(CellState.class);
        cellIcons.put(CellState.EMPTY, new ImageIcon(ImageIO.read(Textures.class.getResource("/images/empty.png"))));
        cellIcons.put(CellState.MINE, new ImageIcon(ImageIO.read(Textures.class.getResource("/images/mine.png"))));
        cellIcons.put(CellState.DIGIT1, new ImageIcon(ImageIO.read(Textures.class.getResource("/images/digit/1.png"))));
        cellIcons.put(CellState.DIGIT2, new ImageIcon(ImageIO.read(Textures.class.getResource("/images/digit/2.png"))));
        cellIcons.put(CellState.DIGIT3, new ImageIcon(ImageIO.read(Textures.class.getResource("/images/digit/3.png"))));
        cellIcons.put(CellState.DIGIT4, new ImageIcon(ImageIO.read(Textures.class.getResource("/images/digit/4.png"))));
        cellIcons.put(CellState.DIGIT5, new ImageIcon(ImageIO.read(Textures.class.getResource("/images/digit/5.png"))));
        cellIcons.put(CellState.DIGIT6, new ImageIcon(ImageIO.read(Textures.class.getResource("/images/digit/6.png"))));
        cellIcons.put(CellState.DIGIT7, new ImageIcon(ImageIO.read(Textures.class.getResource("/images/digit/7.png"))));
        cellIcons.put(CellState.DIGIT8, new ImageIcon(ImageIO.read(Textures.class.getResource("/images/digit/8.png"))));
    }

    private static void loadRedDigitTextures() throws IOException {
        redDigitIcons = new HashMap<>(10);
        redDigitIcons.put('-', new ImageIcon(ImageIO.read(Textures.class.getResource("/images/red_digit/-.png"))));
        redDigitIcons.put('0', new ImageIcon(ImageIO.read(Textures.class.getResource("/images/red_digit/0.png"))));
        redDigitIcons.put('1', new ImageIcon(ImageIO.read(Textures.class.getResource("/images/red_digit/1.png"))));
        redDigitIcons.put('2', new ImageIcon(ImageIO.read(Textures.class.getResource("/images/red_digit/2.png"))));
        redDigitIcons.put('3', new ImageIcon(ImageIO.read(Textures.class.getResource("/images/red_digit/3.png"))));
        redDigitIcons.put('4', new ImageIcon(ImageIO.read(Textures.class.getResource("/images/red_digit/4.png"))));
        redDigitIcons.put('5', new ImageIcon(ImageIO.read(Textures.class.getResource("/images/red_digit/5.png"))));
        redDigitIcons.put('6', new ImageIcon(ImageIO.read(Textures.class.getResource("/images/red_digit/6.png"))));
        redDigitIcons.put('7', new ImageIcon(ImageIO.read(Textures.class.getResource("/images/red_digit/7.png"))));
        redDigitIcons.put('8', new ImageIcon(ImageIO.read(Textures.class.getResource("/images/red_digit/8.png"))));
        redDigitIcons.put('9', new ImageIcon(ImageIO.read(Textures.class.getResource("/images/red_digit/9.png"))));
    }
}
