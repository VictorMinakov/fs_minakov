package ru.cft.focusstart.task3.model.scores;

import java.util.List;

public interface ScoreManager {

    List<Score> read();

    void write(List<Score> scores);

    void clearScores();
}
