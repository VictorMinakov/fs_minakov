package ru.cft.focusstart.task3.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static ru.cft.focusstart.task3.view.LanguageProperties.getProperty;

public class NewPlayerNameView {

    private static final int PLAYER_NAME_TEXT_FIELD_MAX_CHARS = 40;

    private JDialog dialog;
    private JPanel mainPanel;
    private JTextField playerNameTextField;
    private JButton okButton;
    private String playerName;

    public NewPlayerNameView(JFrame parent) {
        createDialog(parent);
        createElements();
        dialog.pack();
        dialog.setLocationRelativeTo(parent);
    }

    String getPlayerName() {
        return playerName;
    }

    void show() {
        dialog.setVisible(true);
    }

    private void createDialog(JFrame parent) {
        dialog = new JDialog(parent);
        dialog.setModal(true);
        dialog.setUndecorated(true);
        dialog.setResizable(false);
    }

    private void createElements() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(3, 1));
        JLabel label = new JLabel(getProperty("new_player_name_view.new_record"));
        mainPanel.add(label);
        createPlayerNameTextField();
        createOKButton();
        dialog.add(mainPanel);
    }

    private void createPlayerNameTextField() {
        playerNameTextField = new JTextField(getProperty("new_player_name_view.default_user"));
        playerNameTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                int textLength = playerNameTextField.getText().length();
                if (textLength >= PLAYER_NAME_TEXT_FIELD_MAX_CHARS) {
                    e.consume();
                }
            }
        });
        mainPanel.add(playerNameTextField);
    }

    private void createOKButton() {
        okButton = new JButton(getProperty("new_player_name_view.button_ok"));
        okButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (okButton.isEnabled()) {
                    playerName = playerNameTextField.getText();
                    dialog.setVisible(false);
                }
            }
        });
        mainPanel.add(okButton);
    }
}
