package ru.cft.focusstart.task3.controller;

import ru.cft.focusstart.task3.controller.timer.MinesweeperTimer;
import ru.cft.focusstart.task3.model.Difficulty;
import ru.cft.focusstart.task3.model.Field;
import ru.cft.focusstart.task3.model.scores.Score;
import ru.cft.focusstart.task3.util.Point;
import ru.cft.focusstart.task3.view.CustomDifficultyViewListener;

import java.awt.event.MouseEvent;
import java.util.List;

public class Controller implements ControllerListener, CustomDifficultyViewListener {

    private Field field;
    private MinesweeperTimer timer;

    public Controller(Field field) {
        this.field = field;
        timer = new MinesweeperTimer(field::updateTimer);
    }

    @Override
    public void cellClick(int mouseButton, Point coordinate) {
        switch (mouseButton) {
            case MouseEvent.BUTTON1:
                field.openCell(coordinate);
                break;
            case MouseEvent.BUTTON2:
                field.cellMiddleClick(coordinate);
                break;
            case MouseEvent.BUTTON3:
                field.cellSetFlag(coordinate);
                break;
            default:
                break;
        }
    }

    @Override
    public void changeDifficulty(Difficulty difficulty) {
        field.changeDifficulty(difficulty);
    }

    @Override
    public void createNewGame() {
        field.createNewGame();
    }

    @Override
    public void startTimer() {
        timer.start();
    }

    @Override
    public void stopTimer() {
        timer.stop();
    }

    @Override
    public void updateScores() {
        field.updateScores();
    }

    @Override
    public void writeScoresToFile(List<Score> scoreList) {
        field.writeScoresToFile(scoreList);
    }

    @Override
    public void clearScores() {
        field.clearScores();
    }
}
