package ru.cft.focusstart.task3.model;

import ru.cft.focusstart.task3.model.observer.MinesweeperObserver;
import ru.cft.focusstart.task3.model.scores.Score;
import ru.cft.focusstart.task3.model.scores.ScoreManager;
import ru.cft.focusstart.task3.util.Point;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Field {

    private static final Random RAND = new Random();

    private MinesweeperObserver observer;
    private Difficulty difficulty;
    private ScoreManager scoreManager;
    private Cell[][] cells;
    private boolean firstMove;
    private boolean endGame;
    private int countOpenedCells;
    private int countNonSelectedMines;
    private int timerCountSeconds;

    public void registerObserver(MinesweeperObserver observer) {
        this.observer = observer;
    }

    public void updateTimer(int timerCountSeconds) {
        this.timerCountSeconds = timerCountSeconds;
        observer.updateTimer(timerCountSeconds);
    }

    public void openCell(Point coordinate) {
        if (endGame) {
            return;
        }
        if (cells[coordinate.y][coordinate.x].isOpened() || cells[coordinate.y][coordinate.x].isFlagInstalled()) {
            return;
        }
        if (firstMove) {
            openCellOnFirstMove(coordinate);
        }
        Map<Point, CellState> cellsToUpdate = new HashMap<>();
        if (cells[coordinate.y][coordinate.x].getState() == CellState.EMPTY) {
            cellsToUpdate = getEmptyCells(coordinate);
        } else {
            if (cells[coordinate.y][coordinate.x].getState() == CellState.MINE) {
                observer.stopTimer();
                endGame = true;
                observer.updateEndGameStatus(EndGameStatus.LOSE, coordinate, getMinesCoordinate());
            } else {
                cells[coordinate.y][coordinate.x].open();
                cellsToUpdate.put(coordinate, cells[coordinate.y][coordinate.x].getState());
            }
        }
        countOpenedCells += cellsToUpdate.size();
        observer.updateCells(cellsToUpdate);
        checkWin();
    }

    public void cellSetFlag(Point coordinate) {
        if (endGame) {
            return;
        }
        if (!cells[coordinate.y][coordinate.x].isOpened()) {
            boolean flagInstalled = cells[coordinate.y][coordinate.x].isFlagInstalled();
            if (flagInstalled) {
                cells[coordinate.y][coordinate.x].setFlagInstalled(false);
                ++countNonSelectedMines;
            } else {
                cells[coordinate.y][coordinate.x].setFlagInstalled(true);
                --countNonSelectedMines;
            }
            observer.updateFlagOnCell(coordinate, !flagInstalled);
            observer.updateCountNonSelectedMines(countNonSelectedMines);
        }
    }

    public void cellMiddleClick(Point coordinate) {
        if (endGame) {
            return;
        }
        Cell cell = cells[coordinate.y][coordinate.x];
        if (cell.isOpened() && CellState.isDigit(cell.getState())) {
            int countNearbyFlags = 0;
            List<Point> cellsToOpen = new ArrayList<>();
            Set<Point> nearbyCells = getNearbyCells(coordinate);
            for (Point cellCoordinate : nearbyCells) {
                if (cells[cellCoordinate.y][cellCoordinate.x].isFlagInstalled()) {
                    ++countNearbyFlags;
                } else {
                    cellsToOpen.add(cellCoordinate);
                }
            }
            System.out.println("count " + countNearbyFlags);
            if (countNearbyFlags >= cell.getState().getDigitValue()) {
                for (Point cellCoordinate : cellsToOpen) {
                    openCell(cellCoordinate);
                }
            }
        }
    }

    public void createNewGame() {
        firstMove = true;
        endGame = false;
        countOpenedCells = 0;
        countNonSelectedMines = difficulty.getCountMines();
        cells = new Cell[difficulty.getCountRows()][difficulty.getCountColumns()];
        generateEmptyCells();
        observer.createNewGame(difficulty.getCountRows(), difficulty.getCountColumns());
        observer.updateCountNonSelectedMines(countNonSelectedMines);
    }

    public void updateScores() {
        List<Score> scoresList = scoreManager.read();
        observer.updateScores(scoresList);
    }

    public void writeScoresToFile(List<Score> scoreList) {
        scoreManager.write(scoreList);
    }

    public void clearScores() {
        scoreManager.clearScores();
        updateScores();
    }

    public void changeDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
        createNewGame();
    }

    public void setScoreManager(ScoreManager scoreManager) {
        this.scoreManager = scoreManager;
    }

    private void openCellOnFirstMove(Point firstClickCoordinate) {
        generateMines(firstClickCoordinate);
        generateDigits();
        firstMove = false;
        observer.startTimer();
    }

    private List<Point> getMinesCoordinate() {
        List<Point> minesCoordinate = new ArrayList<>(difficulty.getCountMines());
        for (int y = 0; y < cells.length; ++y) {
            for (int x = 0; x < cells[y].length; ++x) {
                if (cells[y][x].getState() == CellState.MINE) {
                    minesCoordinate.add(new Point(x, y));
                }
            }
        }
        return minesCoordinate;
    }

    private Map<Point, CellState> getEmptyCells(Point startCoordinate) {
        Set<Point> cellsToVisit = new HashSet<>();
        Set<Point> visitedCells = new HashSet<>();
        Map<Point, CellState> cellsToUpdate = new HashMap<>();
        cellsToVisit.add(startCoordinate);
        while (!cellsToVisit.isEmpty()) {
            removeOpenedAndFlagInstalledCells(cellsToVisit);
            Iterator<Point> iterator = cellsToVisit.iterator();
            while (iterator.hasNext()) {
                Point point = iterator.next();
                Cell currentCell = cells[point.y][point.x];
                if (currentCell.getState() == CellState.EMPTY && !visitedCells.contains(point)) {
                    visitedCells.add(new Point(point.x, point.y));
                    cellsToVisit.addAll(getNearbyCells(point));
                    break;
                }
                cellsToUpdate.put(point, currentCell.getState());
                cells[point.y][point.x].open();
                iterator.remove();
            }
        }
        return cellsToUpdate;
    }

    private void removeOpenedAndFlagInstalledCells(Set<Point> cellsCoordinate) {
        cellsCoordinate.removeIf(cell -> cells[cell.y][cell.x].isFlagInstalled() || cells[cell.y][cell.x].isOpened());
    }

    private boolean isCorrectCoordinate(Point coordinate) {
        int rowsCount = difficulty.getCountRows();
        int columnsCount = difficulty.getCountColumns();
        return (coordinate.x >= 0 && coordinate.x < columnsCount && coordinate.y >= 0 && coordinate.y < rowsCount);
    }

    private Set<Point> getNearbyCells(Point coordinate) {
        return Stream.of(new Point(coordinate.x - 1, coordinate.y - 1),
                new Point(coordinate.x, coordinate.y - 1),
                new Point(coordinate.x + 1, coordinate.y - 1),
                new Point(coordinate.x - 1, coordinate.y),
                new Point(coordinate.x + 1, coordinate.y),
                new Point(coordinate.x - 1, coordinate.y + 1),
                new Point(coordinate.x, coordinate.y + 1),
                new Point(coordinate.x + 1, coordinate.y + 1))
                .filter(this::isCorrectCoordinate)
                .collect(Collectors.toSet());
    }

    private void generateMines(Point firstClickCoordinate) {
        int minesCount = difficulty.getCountMines();
        while (minesCount != 0) {
            int x = RAND.nextInt(difficulty.getCountColumns());
            int y = RAND.nextInt(difficulty.getCountRows());
            if (cells[y][x].getState() == CellState.EMPTY && !(new Point(x, y).equals(firstClickCoordinate))) {
                cells[y][x].setState(CellState.MINE);
                --minesCount;
            }
        }
    }

    private void generateDigits() {
        for (int y = 0; y < cells.length; ++y) {
            for (int x = 0; x < cells[y].length; ++x) {
                Set<Point> nearbyCells = getNearbyCells(new Point(x, y));
                for (Point cellCoordinate : nearbyCells) {
                    if (cells[y][x].getState() == CellState.MINE) {
                        setDigit(cellCoordinate);
                    }
                }
            }
        }
    }

    private void setDigit(Point cellCoordinate) {
        if (isCorrectCoordinate(cellCoordinate)) {
            Cell cell = cells[cellCoordinate.y][cellCoordinate.x];
            if (cell.getState() == CellState.EMPTY) {
                cells[cellCoordinate.y][cellCoordinate.x].setState(CellState.DIGIT1);
            } else if (cell.getState() != CellState.MINE) {
                cell.setState(CellState.valueOfDigitValue(cell.getState().getDigitValue() + 1));
            }
        }
    }

    private void generateEmptyCells() {
        for (int y = 0; y < cells.length; ++y) {
            for (int x = 0; x < cells[y].length; ++x) {
                cells[y][x] = new Cell(CellState.EMPTY);
            }
        }
    }

    private void checkWin() {
        int countMines = difficulty.getCountMines();
        int countCells = difficulty.getCountColumns() * difficulty.getCountRows();
        if (countCells - countOpenedCells == countMines) {
            observer.stopTimer();
            endGame = true;
            observer.updateEndGameStatus(EndGameStatus.WIN, null, getMinesCoordinate());
            checkScores();
        }
    }

    private void checkScores() {
        Map<Difficulty, Integer> defaultDifficulties = new HashMap<>(3);
        defaultDifficulties.put(Difficulty.BEGINNER, 0);
        defaultDifficulties.put(Difficulty.INTERMEDIATE, 1);
        defaultDifficulties.put(Difficulty.EXPERT, 2);

        Integer indexInList = defaultDifficulties.get(difficulty);
        if (indexInList != null) {
            List<Score> scoresList = scoreManager.read();
            Score score = scoresList.get(indexInList);
            if (score.getTime() > timerCountSeconds) {
                scoresList.set(indexInList, new Score(difficulty, timerCountSeconds, null));
                observer.updateScores(scoresList, indexInList);
            }
        }
    }
}
