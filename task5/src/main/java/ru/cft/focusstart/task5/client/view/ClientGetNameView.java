package ru.cft.focusstart.task5.client.view;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static ru.cft.focusstart.task5.common.ChatProperties.getClientLanguageProperty;

public class ClientGetNameView {

    private static final int MIN_WINDOW_WIDTH = 320;
    private static final int MIN_WINDOW_HEIGHT = 70;
    private static final int NAME_TEXT_FIELD_WIDTH = 200;
    private static final int MAX_NAME_LENGTH = 20;

    private JDialog dialog;
    private JPanel mainPanel;
    private JTextField nameTextField;
    private JButton loginButton;
    private String name;

    public ClientGetNameView(JFrame parent) {
        createDialog(parent);
        createElements();
        dialog.pack();
    }

    void show() {
        dialog.setVisible(true);
    }

    String getName() {
        return name;
    }

    private void createDialog(JFrame parent) {
        dialog = new JDialog();
        dialog.setTitle(getClientLanguageProperty("client_get_name_view.title"));
        dialog.setSize(new Dimension(MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGHT));
        dialog.setResizable(false);
        dialog.setModal(true);
        dialog.setLocationRelativeTo(parent);
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(1, 2));
        dialog.add(mainPanel);
    }

    private void createElements() {
        JPanel elementsPanel = new JPanel();
        elementsPanel.setLayout(new FlowLayout());

        createNameTextField();
        createLoginButton();

        elementsPanel.add(nameTextField);
        elementsPanel.add(loginButton);
        mainPanel.add(elementsPanel);
    }

    private void createNameTextField() {
        nameTextField = new JTextField();
        nameTextField.setPreferredSize(new Dimension(NAME_TEXT_FIELD_WIDTH, nameTextField.getMinimumSize().height));
        nameTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkTextLengthInNameTextField();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkTextLengthInNameTextField();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkTextLengthInNameTextField();
            }
        });
        nameTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    loginButtonMouseClicked();
                }
            }
        });
    }

    private void createLoginButton() {
        loginButton = new JButton(getClientLanguageProperty("client_get_name_view.login_button"));
        loginButton.setEnabled(false);
        loginButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                loginButtonMouseClicked();
            }
        });
    }

    private void loginButtonMouseClicked() {
        if (loginButton.isEnabled()) {
            name = nameTextField.getText();
            dialog.setVisible(false);
        }
    }

    private void checkTextLengthInNameTextField() {
        loginButton.setEnabled(!nameTextField.getText().isEmpty());
        Runnable checkingThread = () -> {
            String text = nameTextField.getText();
            if (text.length() > MAX_NAME_LENGTH) {
                nameTextField.setText(text.substring(0, text.length() - 1));
            }
        };
        SwingUtilities.invokeLater(checkingThread);
    }
}
