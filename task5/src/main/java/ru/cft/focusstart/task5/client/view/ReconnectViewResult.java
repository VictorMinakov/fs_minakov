package ru.cft.focusstart.task5.client.view;

enum ReconnectViewResult {

    RECONNECT_START,
    RECONNECT_CANCEL
}
