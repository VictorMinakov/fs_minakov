package ru.cft.focusstart.task5.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focusstart.task5.common.message.ServerMessage;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static ru.cft.focusstart.task5.common.ChatProperties.getServerProperty;

class Server {

    private static final Logger LOGGER = LoggerFactory.getLogger(Server.class);

    private List<ClientHandler> clients;

    Server() {
        LOGGER.info("Server initialization started");
        clients = new CopyOnWriteArrayList<>();
        LOGGER.info("Server initialization finished");
    }

    // suppress sonarLint and idea InfiniteLoopStatement warning
    @SuppressWarnings({"squid:S2189", "InfiniteLoopStatement"})
    void start() {
        int clientTimeout = Integer.parseInt(getServerProperty("client_timeout"));
        try (ServerSocket serverSocket = new ServerSocket(Integer.parseInt(getServerProperty("server_port")))) {
            while (true) {
                Socket clientSocket = serverSocket.accept();
                clientSocket.setSoTimeout(clientTimeout);
                ClientHandler client = new ClientHandler(clientSocket, this);
                clients.add(client);
                new Thread(client).start();
            }
        } catch (IOException e) {
            LOGGER.error("IOException", e);
        }
    }

    synchronized void sendMessageToAllClients(ServerMessage serverMessage) {
        for (ClientHandler client : clients) {
            client.sendMessage(serverMessage);
        }
    }

    void removeClient(ClientHandler client) {
        clients.remove(client);
    }

    String getClientListAsString() {
        StringBuilder clientList = new StringBuilder();
        synchronized (this) {
            for (ClientHandler client : clients) {
                if (client.isConnected()) {
                    clientList.append(client.getUserName()).append("\n");
                }
            }
        }
        return clientList.toString();
    }

    boolean isCorrectUserName(String userName) {
        String clientList = getClientListAsString();
        return Arrays.stream(clientList.split("\n"))
                .noneMatch(name -> name.equals(userName));
    }

    ZoneId getServerZoneId() {
        return ZoneId.of(getServerProperty("server_time_zone"));
    }
}
