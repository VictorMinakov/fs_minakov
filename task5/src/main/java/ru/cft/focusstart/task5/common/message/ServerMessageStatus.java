package ru.cft.focusstart.task5.common.message;

public enum ServerMessageStatus {

    MESSAGE_TEXT,
    MESSAGE_CLIENT_DISCONNECTED,
    MESSAGE_CLIENT_CONNECTED,
    MESSAGE_CLIENTS_LIST,
    MESSAGE_INCORRECT_USERNAME
}
