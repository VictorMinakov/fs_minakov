package ru.cft.focusstart.task5.client;

import ru.cft.focusstart.task5.client.controller.ClientController;
import ru.cft.focusstart.task5.client.model.ClientModel;
import ru.cft.focusstart.task5.client.view.ClientConnectingView;
import ru.cft.focusstart.task5.client.view.ClientGetNameView;
import ru.cft.focusstart.task5.client.view.ClientReconnectView;
import ru.cft.focusstart.task5.client.view.ClientView;

public class ClientMain {

    public static void main(String[] args) {
        ClientModel clientModel = new ClientModel();
        ClientView clientView = new ClientView();
        ClientController clientController = new ClientController(clientModel);

        ClientGetNameView clientGetNameView = new ClientGetNameView(clientView.getFrame());
        ClientConnectingView clientConnectingView = new ClientConnectingView(clientView.getFrame());
        ClientReconnectView clientReconnectView = new ClientReconnectView(clientView.getFrame());

        clientView.setControllerListener(clientController);
        clientView.setClientGetNameView(clientGetNameView);
        clientView.setClientConnectingView(clientConnectingView);
        clientView.setClientReconnectView(clientReconnectView);

        clientModel.registerObserver(clientView);
        clientModel.start();
    }
}
