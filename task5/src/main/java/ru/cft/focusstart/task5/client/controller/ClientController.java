package ru.cft.focusstart.task5.client.controller;

import ru.cft.focusstart.task5.client.model.ClientModel;

public class ClientController implements ControllerListener {

    private ClientModel clientModel;

    public ClientController(ClientModel clientModel) {
        this.clientModel = clientModel;
    }

    @Override
    public void sendText(String text) {
        clientModel.sendText(text);
    }

    @Override
    public void enterChat(String userName) {
        clientModel.enterChat(userName);
    }

    @Override
    public void disconnect() {
        clientModel.disconnect();
    }

    @Override
    public void reconnect() {
        clientModel.reconnect();
    }

    @Override
    public void fullReconnect() {
        clientModel.fullReconnect();
    }
}
