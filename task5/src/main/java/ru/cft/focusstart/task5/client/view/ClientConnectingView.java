package ru.cft.focusstart.task5.client.view;

import javax.swing.*;
import java.awt.*;

import static ru.cft.focusstart.task5.common.ChatProperties.getClientLanguageProperty;

public class ClientConnectingView {

    private static final int MIN_WINDOW_WIDTH = 250;
    private static final int MIN_WINDOW_HEIGHT = 30;

    private JDialog dialog;
    private JPanel mainPanel;

    public ClientConnectingView(JFrame parent) {
        createDialog(parent);
        createElements();
    }

    void setVisible(boolean visible) {
        dialog.setVisible(visible);
    }

    private void createDialog(JFrame parent) {
        dialog = new JDialog();
        dialog.setUndecorated(true);
        dialog.setSize(new Dimension(MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGHT));
        dialog.setResizable(false);
        dialog.setLocationRelativeTo(parent);
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(1, 1));
        dialog.add(mainPanel);
    }

    private void createElements() {
        JPanel elementsPanel = new JPanel();
        elementsPanel.setLayout(new FlowLayout());
        JLabel labelText = new JLabel(getClientLanguageProperty("client_connection_view.text"));
        elementsPanel.add(labelText);
        mainPanel.add(elementsPanel);
    }
}
