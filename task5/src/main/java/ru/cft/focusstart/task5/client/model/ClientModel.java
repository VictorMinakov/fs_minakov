package ru.cft.focusstart.task5.client.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focusstart.task5.common.message.ClientMessage;
import ru.cft.focusstart.task5.common.message.ClientMessageStatus;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

import static ru.cft.focusstart.task5.common.ChatProperties.getClientProperty;

public class ClientModel {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientModel.class);

    private ClientObserver observer;
    private Socket clientSocket;
    private MessageReceiver messageReceiver;
    private MessageSender messageSender;
    private DataInputStream datainputStream;
    private DataOutputStream dataOutputStream;
    private boolean isConnected;
    private String userName;

    public ClientModel() {
        isConnected = false;
    }

    public void start() {
        observer.startView();
    }

    public void registerObserver(ClientObserver observer) {
        this.observer = observer;
    }

    public void sendText(String text) {
        messageSender.sendMessage(new ClientMessage(ClientMessageStatus.CLIENT_TEXT_MESSAGE, text));
    }

    public void reconnect() {
        disconnect();
        initConnection();
        enterChat(userName);
    }

    public void fullReconnect() {
        disconnect();
        start();
    }

    public void enterChat(String userName) {
        initConnection();
        this.userName = userName;
        new Thread(messageReceiver).start();
        messageSender.startSendPingMessages();
        messageSender.sendMessage(new ClientMessage(ClientMessageStatus.CLIENT_CHECK_USERNAME, userName));
    }

    public void disconnect() {
        if (isConnected) {
            messageSender.sendMessage(new ClientMessage(ClientMessageStatus.CLIENT_DISCONNECTED, null));
        }
        try {
            if (clientSocket.isConnected() && !clientSocket.isClosed()) {
                messageReceiver.stopReceiving();
                messageSender.stopSending();
                clientSocket.close();
            }
        } catch (IOException e) {
            LOGGER.error("Disconnect error", e);
        }
    }

    boolean isConnected() {
        return isConnected;
    }

    void setConnected() {
        isConnected = true;
    }

    void connectionError() {
        if (isConnected) {
            isConnected = false;
            messageReceiver.stopReceiving();
            messageSender.stopSending();
        }
        observer.errorConnect();
    }

    private void connect() {
        try {
            clientSocket = new Socket();
            clientSocket.connect(new InetSocketAddress(getClientProperty("server_ip"),
                    Integer.parseInt(getClientProperty("server_port"))));
            datainputStream = new DataInputStream(clientSocket.getInputStream());
            dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
            isConnected = false;
        } catch (IOException e) {
            connectionError();
            LOGGER.error("Error creating input\\output stream from socket", e);
        }
    }

    private void initConnection() {
        connect();
        messageReceiver = new MessageReceiver(datainputStream, observer, this);
        messageSender = new MessageSender(dataOutputStream, this);
    }
}
