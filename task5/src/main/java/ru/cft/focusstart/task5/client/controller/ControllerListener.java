package ru.cft.focusstart.task5.client.controller;

public interface ControllerListener {

    void sendText(String text);

    void enterChat(String name);

    void disconnect();

    void reconnect();

    void fullReconnect();
}
