package ru.cft.focusstart.task5.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class ChatProperties {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChatProperties.class);
    private static final String CLIENT_LANGUAGE_PROPERTIES_FILE = "/client/lang/en_US.properties";
    private static final String CLIENT_PROPERTIES_FILE = "/client/client_configuration.properties";
    private static final String SERVER_PROPERTIES_FILE = "/server/server_configuration.properties";

    private static Properties clientLanguageProperties;
    private static Properties clientProperties;
    private static Properties serverProperties;

    private ChatProperties() {

    }

    public static synchronized String getClientLanguageProperty(String key) {
        if (clientLanguageProperties == null) {
            initProperties();
        }
        return clientLanguageProperties.getProperty(key);
    }

    public static synchronized String getClientProperty(String key) {
        if (clientProperties == null) {
            initProperties();
        }
        return clientProperties.getProperty(key);
    }

    public static synchronized String getServerProperty(String key) {
        if (serverProperties == null) {
            initProperties();
        }
        return serverProperties.getProperty(key);
    }

    private static void initProperties() {
        clientLanguageProperties = new Properties();
        clientProperties = new Properties();
        serverProperties = new Properties();
        readPropertiesFromFile(clientLanguageProperties, CLIENT_LANGUAGE_PROPERTIES_FILE);
        readPropertiesFromFile(clientProperties, CLIENT_PROPERTIES_FILE);
        readPropertiesFromFile(serverProperties, SERVER_PROPERTIES_FILE);
    }

    private static void readPropertiesFromFile(Properties properties, String filename) {
        try (InputStream inputStream = ChatProperties.class.getResourceAsStream(filename);
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream, StandardCharsets.UTF_8)) {
            properties.load(inputStreamReader);
        } catch (IOException e) {
            LOGGER.error("Error loading properties file " + filename, e);
        }
    }
}
