package ru.cft.focusstart.task5.client.view;

import ru.cft.focusstart.task5.client.controller.ControllerListener;
import ru.cft.focusstart.task5.client.model.ClientObserver;
import ru.cft.focusstart.task5.common.message.ServerMessage;
import ru.cft.focusstart.task5.common.message.ServerMessageStatus;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static ru.cft.focusstart.task5.common.ChatProperties.getClientLanguageProperty;
import static ru.cft.focusstart.task5.common.ChatProperties.getClientProperty;

public class ClientView implements ClientObserver {

    private static final int MIN_WINDOW_WIDTH = 800;
    private static final int MIN_WINDOW_HEIGHT = 600;
    private static final int USER_LIST_CELL_WIDTH = 130;
    private static final int MAX_MESSAGE_LENGTH = 100;

    private JFrame frame;
    private JPanel mainPanel;
    private JList<String> userList;
    private JTextArea messageArea;
    private JTextField messageTextField;
    private JButton sendButton;
    private ClientConnectingView clientConnectingView;
    private ClientGetNameView clientGetNameView;
    private ClientReconnectView clientReconnectView;
    private ControllerListener controllerListener;

    public ClientView() {
        createFrame();
        createUsersPanel();
        createMessageAreaPanel();
        createMessageSendArea();
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    public void setControllerListener(ControllerListener controllerListener) {
        this.controllerListener = controllerListener;
    }

    public void setClientGetNameView(ClientGetNameView clientGetNameView) {
        this.clientGetNameView = clientGetNameView;
    }

    public void setClientConnectingView(ClientConnectingView clientConnectingView) {
        this.clientConnectingView = clientConnectingView;
    }

    public void setClientReconnectView(ClientReconnectView clientReconnectView) {
        this.clientReconnectView = clientReconnectView;
    }

    public JFrame getFrame() {
        return frame;
    }

    @Override
    public void printMessage(ServerMessage serverMessage) {
        String clientTime = getReadableConvertedTime(serverMessage.getLocalDateTime());
        String textMessage = null;
        if (serverMessage.getServerMessageStatus() == ServerMessageStatus.MESSAGE_CLIENT_CONNECTED) {
            textMessage = String.format("[%s] %s %s%n",
                    clientTime,
                    serverMessage.getUserName(),
                    getClientLanguageProperty("client_view.connected"));
        }
        if (serverMessage.getServerMessageStatus() == ServerMessageStatus.MESSAGE_CLIENT_DISCONNECTED) {
            textMessage = String.format("[%s] %s %s%n",
                    clientTime,
                    serverMessage.getUserName(),
                    getClientLanguageProperty("client_view.disconnected"));
        }
        if (serverMessage.getServerMessageStatus() == ServerMessageStatus.MESSAGE_TEXT) {
            textMessage = String.format("[%s] <%s> %s%n",
                    clientTime,
                    serverMessage.getUserName(),
                    serverMessage.getText());
        }
        messageArea.append(textMessage);
    }

    @Override
    public void startView() {
        messageTextField.setEnabled(false);
        sendButton.setEnabled(false);
        clientGetNameView.show();
        String userName = clientGetNameView.getName();
        if (userName == null) {
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        }
        frame.setVisible(true);
        clientConnectingView.setVisible(true);
        controllerListener.enterChat(userName);
    }

    @Override
    public void successConnect() {
        messageTextField.setEnabled(true);
        sendButton.setEnabled(true);
        clientConnectingView.setVisible(false);
    }

    @Override
    public synchronized void errorConnect() {
        messageTextField.setEnabled(false);
        sendButton.setEnabled(false);
        clientConnectingView.setVisible(false);
        clientReconnectView.setErrorMessage(getClientLanguageProperty("client_view.message_connect_error"));
        clientReconnectView.show();
        if (clientReconnectView.getReconnectViewResult() == ReconnectViewResult.RECONNECT_START) {
            controllerListener.reconnect();
        } else {
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        }
    }

    @Override
    public void errorUserName() {
        messageTextField.setEnabled(false);
        sendButton.setEnabled(false);
        clientConnectingView.setVisible(false);
        clientReconnectView.setErrorMessage(getClientLanguageProperty("client_view.message_incorrect_name"));
        clientReconnectView.show();
        if (clientReconnectView.getReconnectViewResult() == ReconnectViewResult.RECONNECT_START) {
            controllerListener.fullReconnect();
        } else {
            frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
        }
    }

    @Override
    public void updateClientList(String clientList) {
        String[] clients = clientList.split("\n");
        userList.setListData(clients);
    }

    private String getReadableConvertedTime(LocalDateTime localDateTime) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
        ZoneId zoneId = ZoneId.of(getClientProperty("server_time_zone"));
        ZonedDateTime serverTime = ZonedDateTime.of(localDateTime, zoneId);
        return serverTime.withZoneSameInstant(ZoneId.systemDefault())
                .toLocalTime().format(dateTimeFormatter);
    }

    private void createFrame() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle(getClientLanguageProperty("client_view.title"));
        frame.setMinimumSize(new Dimension(MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGHT));
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        frame.add(mainPanel);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                controllerListener.disconnect();
            }
        });
    }

    private void createUsersPanel() {
        JPanel userListPanel = new JPanel();
        userListPanel.setLayout(new BorderLayout());
        createUserList();
        JScrollPane userListScrollPane = new JScrollPane(userList);
        userListPanel.add(userListScrollPane, BorderLayout.CENTER);
        mainPanel.add(userListPanel, BorderLayout.WEST);
    }

    private void createMessageAreaPanel() {
        JPanel messageAreaPanel = new JPanel();
        messageAreaPanel.setLayout(new BorderLayout());
        createMessageArea();
        JScrollPane textAreaScrollPane = new JScrollPane(messageArea);
        messageAreaPanel.add(textAreaScrollPane, BorderLayout.CENTER);
        mainPanel.add(messageAreaPanel, BorderLayout.CENTER);
    }

    private void createMessageSendArea() {
        JPanel messageSendArea = new JPanel();
        messageSendArea.setLayout(new BorderLayout());

        createMessageTextField();
        createSendButton();

        messageSendArea.add(messageTextField, BorderLayout.CENTER);
        messageSendArea.add(sendButton, BorderLayout.EAST);
        mainPanel.add(messageSendArea, BorderLayout.SOUTH);
    }

    private void createUserList() {
        userList = new JList<>();
        userList.setLayoutOrientation(JList.VERTICAL);
        userList.setFixedCellWidth(USER_LIST_CELL_WIDTH);
    }

    private void createMessageArea() {
        messageArea = new JTextArea();
        messageArea.setEnabled(false);
        messageArea.setDisabledTextColor(Color.BLACK);
        DefaultCaret caret = (DefaultCaret) messageArea.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    }

    private void createMessageTextField() {
        messageTextField = new JTextField();
        messageTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkTextLengthInMessageTextField();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkTextLengthInMessageTextField();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkTextLengthInMessageTextField();
            }
        });
        messageTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    sendMessage();
                }
            }
        });
    }

    private void createSendButton() {
        sendButton = new JButton(getClientLanguageProperty("client_view.send_button"));
        sendButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                sendMessage();
            }
        });
    }

    private void sendMessage() {
        if (sendButton.isEnabled()) {
            controllerListener.sendText(messageTextField.getText());
            messageTextField.setText("");
        }
    }

    private void checkTextLengthInMessageTextField() {
        sendButton.setEnabled(!messageTextField.getText().isEmpty());
        Runnable checkingThread = () -> {
            String text = messageTextField.getText();
            if (text.length() > MAX_MESSAGE_LENGTH) {
                messageTextField.setText(text.substring(0, text.length() - 1));
            }
        };
        SwingUtilities.invokeLater(checkingThread);
    }
}
