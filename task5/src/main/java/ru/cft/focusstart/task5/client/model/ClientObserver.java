package ru.cft.focusstart.task5.client.model;

import ru.cft.focusstart.task5.common.message.ServerMessage;

public interface ClientObserver {

    void printMessage(ServerMessage serverMessage);

    void startView();

    void successConnect();

    void errorConnect();

    void errorUserName();

    void updateClientList(String clientList);
}
