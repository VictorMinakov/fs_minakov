package ru.cft.focusstart.task5.common.message;

public class ClientMessage {

    private ClientMessageStatus clientMessageStatus;
    private String message;

    public ClientMessage(ClientMessageStatus clientMessageStatus, String message) {
        this.clientMessageStatus = clientMessageStatus;
        this.message = message;
    }

    public ClientMessageStatus getClientMessageStatus() {
        return clientMessageStatus;
    }

    public String getMessage() {
        return message;
    }
}
