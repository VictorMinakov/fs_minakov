package ru.cft.focusstart.task5.common.message;

public enum ClientMessageStatus {

    CLIENT_DISCONNECTED,
    CLIENT_PING_MESSAGE,
    CLIENT_TEXT_MESSAGE,
    CLIENT_CHECK_USERNAME
}
