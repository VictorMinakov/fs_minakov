package ru.cft.focusstart.task5.client.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static ru.cft.focusstart.task5.common.ChatProperties.getClientLanguageProperty;

public class ClientReconnectView {

    private static final int MIN_WINDOW_WIDTH = 350;
    private static final int MIN_WINDOW_HEIGHT = 100;

    private JDialog dialog;
    private JPanel mainPanel;
    private JLabel errorMessageLabel;
    private JButton exitButton;
    private JButton reconnectButton;
    private ReconnectViewResult reconnectViewResult;

    public ClientReconnectView(JFrame parent) {
        reconnectViewResult = ReconnectViewResult.RECONNECT_CANCEL;
        createDialog(parent);
        createElements();
    }

    void show() {
        dialog.setVisible(true);
    }

    ReconnectViewResult getReconnectViewResult() {
        return reconnectViewResult;
    }

    void setErrorMessage(String errorMessage) {
        errorMessageLabel.setText(errorMessage);
    }

    private void createDialog(JFrame parent) {
        dialog = new JDialog();
        dialog.setSize(new Dimension(MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGHT));
        dialog.setResizable(false);
        dialog.setLocationRelativeTo(parent);
        dialog.setModal(true);
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(2, 2));
        dialog.add(mainPanel);
        dialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                reconnectViewResult = ReconnectViewResult.RECONNECT_CANCEL;
            }
        });
    }

    private void createElements() {
        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new FlowLayout());
        errorMessageLabel = new JLabel();
        labelPanel.add(errorMessageLabel);
        mainPanel.add(labelPanel);

        createButtons();
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new FlowLayout());
        buttonsPanel.add(exitButton);
        buttonsPanel.add(reconnectButton);
        mainPanel.add(buttonsPanel);
    }

    private void createButtons() {
        reconnectButton = new JButton(getClientLanguageProperty("client_reconnect_view.retry"));
        reconnectButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                reconnectViewResult = ReconnectViewResult.RECONNECT_START;
                dialog.setVisible(false);
            }
        });
        exitButton = new JButton(getClientLanguageProperty("client_reconnect_view.cancel"));
        exitButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                reconnectViewResult = ReconnectViewResult.RECONNECT_CANCEL;
                dialog.setVisible(false);
            }
        });
    }
}
