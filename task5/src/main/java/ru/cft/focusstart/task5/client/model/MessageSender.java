package ru.cft.focusstart.task5.client.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focusstart.task5.common.message.ClientMessage;
import ru.cft.focusstart.task5.common.message.ClientMessageStatus;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import static ru.cft.focusstart.task5.common.ChatProperties.getClientProperty;

class MessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageReceiver.class);

    private DataOutputStream dataOutputStream;
    private ClientModel clientModel;
    private Gson gson;
    private Timer pingMessagesTimer;

    MessageSender(DataOutputStream dataOutputStream, ClientModel clientModel) {
        this.dataOutputStream = dataOutputStream;
        this.clientModel = clientModel;
        gson = new GsonBuilder().create();
    }

    void startSendPingMessages() {
        int waitMessagesInterval = Integer.parseInt(getClientProperty("client_ping_messages_interval"));
        pingMessagesTimer = new Timer();
        pingMessagesTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                sendMessage(new ClientMessage(ClientMessageStatus.CLIENT_PING_MESSAGE, null));
            }
        }, 0, waitMessagesInterval);
    }

    void stopSending() {
        pingMessagesTimer.cancel();
    }

    synchronized void sendMessage(ClientMessage clientMessage) {
        try {
            dataOutputStream.writeUTF(gson.toJson(clientMessage));
            dataOutputStream.flush();
        } catch (IOException e) {
            clientModel.connectionError();
            LOGGER.error("Error of send message", e);
        }
    }
}
