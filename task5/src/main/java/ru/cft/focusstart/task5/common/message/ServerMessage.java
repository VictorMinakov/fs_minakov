package ru.cft.focusstart.task5.common.message;

import java.time.LocalDateTime;

public class ServerMessage {

    private String text;
    private String userName;
    private ServerMessageStatus serverMessageStatus;
    private LocalDateTime localDateTime;

    public ServerMessage(ServerMessageStatus serverMessageStatus, String text, String userName,
                         LocalDateTime localDateTime) {
        this.serverMessageStatus = serverMessageStatus;
        this.text = text;
        this.userName = userName;
        this.localDateTime = localDateTime;
    }

    public ServerMessageStatus getServerMessageStatus() {
        return serverMessageStatus;
    }

    public String getText() {
        return text;
    }

    public String getUserName() {
        return userName;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }
}
