package ru.cft.focusstart.task5.client.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focusstart.task5.common.message.ServerMessage;

import java.io.DataInputStream;
import java.io.IOException;

public class MessageReceiver implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageReceiver.class);

    private DataInputStream dataInputStream;
    private ClientObserver clientObserver;
    private ClientModel clientModel;
    private Gson gson;
    private boolean stopReceiving;

    MessageReceiver(DataInputStream dataInputStream, ClientObserver clientObserver, ClientModel clientModel) {
        this.dataInputStream = dataInputStream;
        this.clientObserver = clientObserver;
        this.clientModel = clientModel;
        gson = new GsonBuilder().create();
        stopReceiving = false;
    }

    @Override
    public void run() {
        while (!stopReceiving) {
            try {
                ServerMessage serverMessage = gson.fromJson(dataInputStream.readUTF(), ServerMessage.class);
                switch (serverMessage.getServerMessageStatus()) {
                    case MESSAGE_TEXT:
                    case MESSAGE_CLIENT_DISCONNECTED:
                    case MESSAGE_CLIENT_CONNECTED:
                        if (!clientModel.isConnected()) {
                            clientModel.setConnected();
                            clientObserver.successConnect();
                        }
                        clientObserver.printMessage(serverMessage);
                        break;
                    case MESSAGE_CLIENTS_LIST:
                        clientObserver.updateClientList(serverMessage.getText());
                        break;
                    case MESSAGE_INCORRECT_USERNAME:
                        System.out.println("2222");
                        clientObserver.errorUserName();
                        break;
                }
            } catch (IOException e) {
                clientModel.connectionError();
                LOGGER.error("Error with receiving message", e);
            }
        }
    }

    void stopReceiving() {
        stopReceiving = true;
    }
}
