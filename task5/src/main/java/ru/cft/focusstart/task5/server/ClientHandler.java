package ru.cft.focusstart.task5.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.cft.focusstart.task5.common.message.ClientMessage;
import ru.cft.focusstart.task5.common.message.ServerMessage;
import ru.cft.focusstart.task5.common.message.ServerMessageStatus;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.time.LocalDateTime;

class ClientHandler implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientHandler.class);
    private final Server server;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;
    private Socket socket;
    private Gson gson;
    private boolean isConnected;
    private String userName;

    ClientHandler(Socket socket, Server server) {
        isConnected = false;
        this.socket = socket;
        this.server = server;
        gson = new GsonBuilder().create();
        try {
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            dataInputStream = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            disconnect();
            LOGGER.error("Error creating input\\output stream from socket", e);
        }
    }

    @Override
    public void run() {
        while (!socket.isClosed()) {
            try {
                ClientMessage clientMessage = gson.fromJson(dataInputStream.readUTF(), ClientMessage.class);
                parseClientMessageAndSendToAllClients(clientMessage);
            } catch (IOException e) {
                disconnect();
                LOGGER.error("Error with receiving message", e);
            }
        }
    }

    String getUserName() {
        return userName;
    }

    boolean isConnected() {
        return isConnected;
    }

    void sendMessage(ServerMessage serverMessage) {
        if (isConnected || serverMessage.getServerMessageStatus() == ServerMessageStatus.MESSAGE_INCORRECT_USERNAME) {
            try {
                dataOutputStream.writeUTF(gson.toJson(serverMessage));
            } catch (IOException e) {
                disconnect();
                LOGGER.error("Error with sending message", e);
            }
        }
    }

    private void parseClientMessageAndSendToAllClients(ClientMessage clientMessage) {
        switch (clientMessage.getClientMessageStatus()) {
            case CLIENT_TEXT_MESSAGE:
                LOGGER.trace("Client \"{}\" sends message: {}", userName, clientMessage.getMessage());
                server.sendMessageToAllClients(new ServerMessage(ServerMessageStatus.MESSAGE_TEXT,
                        clientMessage.getMessage(), userName, LocalDateTime.now(server.getServerZoneId())));
                break;
            case CLIENT_DISCONNECTED:
                disconnect();
                break;
            case CLIENT_CHECK_USERNAME:
                checkCorrectUserName(clientMessage.getMessage());
                break;
            default:
                break;
        }
    }

    private void checkCorrectUserName(String userName) {
        synchronized (server) {
            if (server.isCorrectUserName(userName)) {
                isConnected = true;
                this.userName = userName;
                sendClientList();
                LOGGER.info("Client \"{}\" connected. {}", userName, socket);
                server.sendMessageToAllClients(new ServerMessage(ServerMessageStatus.MESSAGE_CLIENT_CONNECTED,
                        null, userName, LocalDateTime.now(server.getServerZoneId())));
            } else {
                sendMessage(new ServerMessage(ServerMessageStatus.MESSAGE_INCORRECT_USERNAME,
                        null, userName, LocalDateTime.now(server.getServerZoneId())));
                disconnect();
            }
        }

    }

    private void sendClientList() {
        String clientListAsString = server.getClientListAsString();
        server.sendMessageToAllClients(new ServerMessage(
                ServerMessageStatus.MESSAGE_CLIENTS_LIST, clientListAsString,
                null, LocalDateTime.now(server.getServerZoneId())));
    }

    private void disconnect() {
        try {
            server.removeClient(this);
            if (isConnected) {
                LOGGER.info("Client \"{}\" disconnected", userName);
                server.sendMessageToAllClients(new ServerMessage(ServerMessageStatus.MESSAGE_CLIENT_DISCONNECTED,
                        null, userName, LocalDateTime.now(server.getServerZoneId())));
                sendClientList();
            }

            socket.close();
        } catch (IOException e) {
            LOGGER.error("Disconnect error", e);
        }
    }
}
