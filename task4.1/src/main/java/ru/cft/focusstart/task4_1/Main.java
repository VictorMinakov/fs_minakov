package ru.cft.focusstart.task4_1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws InterruptedException {
        int countThreads = Integer.parseInt(TaskProperties.getProperty("count_threads"));
        long countElements = Long.parseLong(TaskProperties.getProperty("count_elements"));
        List<Future<Double>> futureList = new ArrayList<>(countThreads);
        ExecutorService executorService = Executors.newFixedThreadPool(countThreads);
        for (int i = 0; i < countThreads; ++i) {
            long startNumber = i * (countElements / countThreads) + 1;
            long countNumbers = countElements / countThreads;
            futureList.add(executorService.submit(new Task(startNumber, countNumbers)));
        }
        executorService.shutdown();
        double result = 0;
        try {
            for (Future<Double> future : futureList) {
                result += future.get();
            }
        } catch (ExecutionException e) {
            LOGGER.error("ExecutionException", e);
        }
        LOGGER.info("Result = {}", result);
    }
}
