package ru.cft.focusstart.task4_1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

public class Task implements Callable<Double> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Task.class);

    private long startNumber;
    private long countNumbers;
    private double result = 0;

    Task(long startNumber, long countNumbers) {
        this.startNumber = startNumber;
        this.countNumbers = countNumbers;
    }

    @Override
    public Double call() {
        LOGGER.info("Thread {} started with startNumber {} and endNumber {}",
                Thread.currentThread().getName(), startNumber, startNumber + countNumbers);
        for (long i = startNumber; i < startNumber + countNumbers; ++i) {
            result += Math.pow(-1, i + 1.0) * (4.0 / (2.0 * i - 1.0));
        }
        LOGGER.info("Thread {} finished", Thread.currentThread().getName());
        return result;
    }
}
