package ru.cft.focusstart.task1.table;

import java.util.Arrays;

public final class MultiplicationTable {

    public static final int MIN_TABLE_SIZE = 1;
    public static final int MAX_TABLE_SIZE = 32;

    private static final int STRING_BUILDER_BUFFER_SIZE = 100;

    private int tableSize;
    private int cellSize;
    private int minCellSize;

    public MultiplicationTable(final int tableSize) {
        if (tableSize < MIN_TABLE_SIZE || tableSize > MAX_TABLE_SIZE) {
            throw new IllegalArgumentException("Incorrect table size.");
        }
        this.tableSize = tableSize;
        cellSize = String.valueOf(tableSize * tableSize).length();
        minCellSize = String.valueOf(tableSize).length();
    }

    public void print() {
        printHeader();
        printLineSeparator();
        printTable();
    }

    private void printHeader() {
        StringBuilder header = new StringBuilder(STRING_BUILDER_BUFFER_SIZE);
        header.append(getSymbolString(minCellSize, ' '));
        for (int i = 1; i <= tableSize; ++i) {
            String currentValue = String.valueOf(i);
            header.append("|");
            header.append(getSymbolString(cellSize - currentValue.length(), ' '));
            header.append(currentValue);
        }
        System.out.println(header);
    }

    private void printLeftPart(int value) {
        int leftPartSize = minCellSize - String.valueOf(value).length();
        String leftPart = getSymbolString(leftPartSize, ' ') + value;
        System.out.print(leftPart);
    }

    private void printLineSeparator() {
        StringBuilder line = new StringBuilder(STRING_BUILDER_BUFFER_SIZE);
        line.append(getSymbolString(minCellSize, '-'));
        for (int i = 1; i <= tableSize; ++i) {
            line.append('+');
            line.append(getSymbolString(cellSize, '-'));
        }
        System.out.println(line);
    }

    private void printTable() {
        for (int i = 1; i <= tableSize; ++i) {
            printLeftPart(i);
            StringBuilder line = new StringBuilder(STRING_BUILDER_BUFFER_SIZE);
            for (int j = 1; j <= tableSize; ++j) {
                String currentValue = String.valueOf(i * j);
                line.append("|");
                line.append(getSymbolString(cellSize - currentValue.length(), ' '));
                line.append(currentValue);
            }
            System.out.println(line);
            printLineSeparator();
        }
    }

    private String getSymbolString(int length, char symbol) {
        char[] str = new char[length];
        Arrays.fill(str, symbol);
        return String.valueOf(str);
    }
}
