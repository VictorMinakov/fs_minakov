package ru.cft.focusstart.task1;

import ru.cft.focusstart.task1.table.MultiplicationTable;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String size = MultiplicationTable.MIN_TABLE_SIZE + " <= size <= " + MultiplicationTable.MAX_TABLE_SIZE;
        System.out.println("Enter table size. Size must be " + size + ".");
        try {
            Scanner scanner = new Scanner(System.in);
            if (scanner.hasNextInt()) {
                int tableSize = scanner.nextInt();
                MultiplicationTable table = new MultiplicationTable(tableSize);
                table.print();
            }
            else {
                System.out.println("Incorrect table size.");
            }
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}
