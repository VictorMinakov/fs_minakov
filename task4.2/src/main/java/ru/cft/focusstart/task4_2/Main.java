package ru.cft.focusstart.task4_2;

public class Main {

    public static void main(String[] args) {
        int countProducers = Integer.parseInt(TaskProperties.getProperty("N"));
        int countConsumers = Integer.parseInt(TaskProperties.getProperty("M"));

        Storage storage = new Storage();

        for (int i = 0; i < countProducers; ++i) {
            Thread thread = new Thread(new Producer(storage));
            thread.setName("Producer " + i);
            thread.start();
        }

        for (int i = 0; i < countConsumers; ++i) {
            Thread thread = new Thread(new Consumer(storage));
            thread.setName("Consumer " + i);
            thread.start();
        }
    }
}
