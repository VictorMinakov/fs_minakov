package ru.cft.focusstart.task4_2;

class Resource {

    private String name;

    Resource(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }
}
