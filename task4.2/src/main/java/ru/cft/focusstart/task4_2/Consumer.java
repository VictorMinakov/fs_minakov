package ru.cft.focusstart.task4_2;

class Consumer implements Runnable {

    private Storage storage;

    Consumer(Storage storage) {
        this.storage = storage;
    }

    // suppress sonarLint and idea InfiniteLoopStatement warning
    @SuppressWarnings({"squid:S2189", "InfiniteLoopStatement"})
    @Override
    public void run() {
        int delay = Integer.parseInt(TaskProperties.getProperty("tM"));
        try {
            while (true) {
                storage.removeResource();
                Thread.sleep(delay);
            }
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
        }
    }
}
