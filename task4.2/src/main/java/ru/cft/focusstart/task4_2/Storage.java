package ru.cft.focusstart.task4_2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.Queue;

class Storage {

    private static final Logger LOGGER = LoggerFactory.getLogger(Storage.class);

    private Queue<Resource> resources;
    private int countMaxResources;

    Storage() {
        countMaxResources = Integer.valueOf(TaskProperties.getProperty("S"));
        resources = new LinkedList<>();
    }

    synchronized void addResource(Resource resource) throws InterruptedException {
        while (resources.size() == countMaxResources) {
            wait();
        }
        notifyAll();
        resources.add(resource);
        LOGGER.info("[{}] Resource {} produced by {}. Storage contains {} resources",
                LocalDateTime.now(), resource.getName(), Thread.currentThread().getName(), resources.size());
    }

    synchronized void removeResource() throws InterruptedException {
        while (resources.isEmpty()) {
            wait();
        }
        notifyAll();
        LOGGER.info("[{}] Resource {} consumed by {}. Storage contains {} resources",
                LocalDateTime.now(), resources.remove().getName(), Thread.currentThread().getName(), resources.size());
    }
}
