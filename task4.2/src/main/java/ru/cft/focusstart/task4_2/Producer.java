package ru.cft.focusstart.task4_2;

class Producer implements Runnable {

    private static volatile int currentResource = 0;

    private Storage storage;

    Producer(Storage storage) {
        this.storage = storage;
    }

    private static synchronized Resource createResource() {
        return new Resource(String.valueOf(++currentResource));
    }

    // suppress sonarLint and idea InfiniteLoopStatement warning
    @SuppressWarnings({"squid:S2189", "InfiniteLoopStatement"})
    @Override
    public void run() {
        int delay = Integer.parseInt(TaskProperties.getProperty("tN"));
        try {
            while (true) {
                storage.addResource(createResource());
                Thread.sleep(delay);
            }
        } catch (InterruptedException ignored) {
            Thread.currentThread().interrupt();
        }
    }
}
